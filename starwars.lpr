{$mode fpc}
PROGRAM starwars;
uses graph,crt,dos,eric;
label 1,2,3;
var a,b,x,y,mm,xmm,ymm,m1,xm1,ym1,testx,testy,xx,yy,color,xxxx,yyyy,bouc,aaa,toch,debut,fin,df,
debut2,fin2,df2,tps,nbouc,align,vit,dff,some,

                     m2,ym2,xm2,
                     m3,ym3,xm3,
                     m4,xm4,ym4,
                     m5,xm5,ym5,
                     m6,ym6,xm6,
                     m7,ym7,xm7,
                     m8,xm8,ym8,

                     em1,xem1,yem1,
                     em2,yem2,xem2,
                     em3,yem3,xem3,
                     em4,xem4,yem4,
                     em5,xem5,yem5,
                     em6,yem6,xem6,
                     em7,yem7,xem7,
                     em8,xem8,yem8,
                     em9,xem9,yem9,
                     em10,yem10,xem10:integer;
                    mots:string;
                    cth:char;
PROCEDURE logo;
          BEGIN
          setcolor(1);
          line(470,128,350,128);line(350,128,505,163);line(470,128,505,163);
          line(475,126,510,20); line(510,20,510,161); line(475,126,510,161);
          line(515,115,515,170); line(515,115,580,250); line(515,170,580,250);
          arc2(373,392,90,270,16); arc2(530,392,270,90,16);
          line(373,376,530,376);  line(373,408,530,408);
          TEXTCOLOR (1);
          setcolor(1);
          settextstyle(10,0,7);
          outtextxy(100,150,'TAKAMI');
          settextstyle(1,0,4);
          outtextxy(370,370,'Production');
          setfillstyle(2,1);
          floodfill(0,0,1);
          floodfill(195,235,1);
          floodfill(335,235,1);
          delay(2500);
          closegraph;
          opengraph;
          setcolor(15);
          settextstyle(10,0,3);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(7);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(8);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(0);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          settextstyle(0,0,0);
         END;

PROCEDURE scor;
var
  F,G: Text;
  Ch: Char;
  mot:string;
begin
  { Get file to read from command line }
  textcolor(15);
  closegraph;
  clrscr;
  writeln('Meilleur score:');
  Assign(F,'nom.txt');
  Assign(G,'scor.txt');
  Reset(F);
  Read(F, mot);
  Write(mot);  { Dump text file }
  write('.....');
  Reset(G);
  read(g,mots);
  write(mots);
  write(' secondes');
  readkey;
  opengraph;
end;

PROCEDURE recor;
var
  F,G: Text;
  Ch: Char;
  nom,score:string;
begin
  { Get file to read from command line }
  textcolor(15);
  closegraph;
  clrscr;
  writeln('.......Nouveau record......');
  writeln('Nom du vainqueur:');
  readln(nom);
  Assign(F,'nom.txt');
  Assign(G,'scor.txt');
  append(f);
  rewrite(f);
  write(F,nom);
  close(f);
  append(g);
  rewrite(g);
  write(g,chaine(dff));
  close(g);
  opengraph;
end;

PROCEDURE titre;
BEGIN
setcolor(10);
settextstyle(10,0,3);
outtextxy(210,100,'STARWARS');
settextstyle(0,0,0);
outtextxy(250,170,'UNE SEULE REGLE');
Outtextxy(250,180,'   SURVIVRE');
outtextxy(220,210,'     V: turbo on/off');
Outtextxy(220,220,'     B: ecran protecteur');
outtextxy(220,230,'     N: tir croise');
outtextxy(220,240,'     C: stabilisateur');
outtextxy(220,250,'espace: tir normal');
outtextxy(220,260,'entree: pause');
outtextxy(190,280,'Votre vaisseau ne possede que 2 ecrans');
outtextxy(190,290,'   protecteurs qui ne dure que 2');
outtextxy(190,300,'        secondes chacun.');
delay(500);
readkey;
END;

PROCEDURE parametre;
          BEGIN
          x:=320; y:=400; a:=0; xm1:=0; ym1:=0; m1:=0; aaa:=0;
                                xm2:=0; ym2:=0; m2:=0; tps:=0;
                                xm3:=0; ym3:=0; m3:=0; nbouc:=2;
                                xm4:=0; ym4:=0; m4:=0; align:=0;
                                xm5:=0; ym5:=0; m5:=0; vit:=2;
                                xm6:=0; ym6:=0; m6:=0;
                                xm7:=0; ym7:=0; m7:=0;
                                xm8:=0; ym8:=0; m8:=0;
                                em1:=0; em2:=0; em3:=0;
                                em4:=0; em5:=0; bouc:=0;
          END;

PROCEDURE vaisseau;
          begin
          setcolor(15);
          if vit=4 then begin setcolor(15); end;
          if tps=1 then begin setcolor(14); end;
          if b=1 then begin setcolor(0); end;
          line(x,y+3,x,y+6);
          line(x-1,y+4,x-1,y+6);
          line(x+1,y+4,x+1,y+6);
          setcolor(9);
          if vit=4 then begin setcolor(12); end;
          if tps=1 then begin setcolor(14); end;
          if b=1 then begin setcolor(0); end;
          line(x,y,x,y+2);
          line(x,y+7,x,y+20);
          line(x+1,y+1,x+1,y+3);
          line(x-1,y+1,x-1,y+3);
          line(x+1,y+7,x+1,y+20);
          line(x-1,y+7,x-1,y+20);
          line(x+2,y+2,x+1,y+20);
          line(x-2,y+2,x-1,y+20);
          line(x+3,y+4,x+1,y+20);
          line(x-3,y+4,x-1,y+20);
          setcolor(12);
          if vit=4 then begin setcolor(10); end;
          if tps=1 then begin setcolor(14); end;
          if b=1 then begin setcolor(0); end;
          line(x+5,y+8,x+4,y+19);
          line(x-5,y+8,x-4,y+19);
          line(x+6,y+9,x+6,y+19);
          line(x-6,y+9,x-6,y+19);
          line(x+7,y+9,x+8,y+19);
          line(x-7,y+9,x-8,y+19);
          setcolor(10);
          if vit=4 then begin setcolor(10); end;
          if tps=1 then begin setcolor(14); end;
          if b=1 then begin setcolor(0); end;
          line(x+3,y+8,x+3,y+13);
          line(x-3,y+8,x-3,y+13);
          end;

FUNCTION missile(t,xt,yt:INTEGER):STRING;
          begin
          if t=1 then
          begin
          if xt=0 then
          begin
          xt:=x;
          yt:=y-10;
          end;
          setcolor(0);
          line(xt,yt,xt,yt+2);
          line(xt+1,yt+1,xt+1,yt+3);
          line(xt-1,yt+1,xt-1,yt+3);
          yt:=yt-4;
          setcolor(14);
          line(xt,yt,xt,yt+2);
          line(xt+1,yt+1,xt+1,yt+3);
          line(xt-1,yt+1,xt-1,yt+3);
          end;
          if yt<-10 then
          begin
          t:=0; xt:=0;
          end;
          mm:=t; xmm:=xt; ymm:=yt;
          end;

FUNCTION obser(xxx,yyy:integer):integer;
          var pc1,pc2,pc3,pc4,pc5,pc6,pc7: Word;
              tt:integer;
              ch:char;
          BEGIN
          for tt:=0 to 6 do
          begin
          pc1:=getpixel(xxx-2,yyy+1-tt);
          pc2:=getpixel(xxx-1,yyy+1-tt);
          pc3:=getpixel(xxx,yyy+1-tt);
          pc4:=getpixel(xxx+1,yyy+1-tt);
          pc5:=getpixel(xxx+2,yyy+1-tt);
          if ((pc1=12)or(pc1=14)or(pc1=9)or(pc1=10)or
              (pc2=12)or(pc2=14)or(pc2=9)or(pc2=10)or
              (pc3=12)or(pc3=14)or(pc3=9)or(pc3=10)or
              (pc4=12)or(pc4=14)or(pc4=9)or(pc4=10)or
              (pc5=12)or(pc5=14)or(pc5=9)or(pc5=10))
              then
              begin
              break;
              end;
          end;
          if ((pc1=12)or(pc1=9)or(pc1=10)or
              (pc2=12)or(pc2=9)or(pc2=10)or
              (pc3=12)or(pc3=9)or(pc3=10)or
              (pc4=12)or(pc4=9)or(pc4=10)or
              (pc5=12)or(pc5=9)or(pc5=10)) then
              begin
              while toch=0 do
              begin
              if keypressed then
              begin
              dff:=df;
              ch:=readkey;
              if ch=#13 then
              begin
              toch:=1;
              end;
              end;
              end;
              end;

              if ((pc1=14)or(pc2=14)or(pc3=14)or(pc4=14)or(pc5=14)) then
              begin
              obser:=1;
              end
              else
              begin
              obser:=0;
              end;
          END;



PROCEDURE ennemis1(xxx,yyy:integer);
          begin
          if aaa=0 then
          begin
          setcolor(15);
          line(xxx,yyy-4,xxx,yyy-12);
          line(xxx-1,yyy-4,xxx-1,yyy-10);
          line(xxx+1,yyy-4,xxx+1,yyy-10);
          line(xxx-2,yyy-7,xxx-2,yyy-12);
          line(xxx+2,yyy-7,xxx+2,yyy-12);
          setcolor(4);
          line(xxx,yyy,xxx,yyy-3);
          line(xxx-1,yyy-1,xxx-1,yyy-3);
          line(xxx+1,yyy-1,xxx+1,yyy-3);
          end;
          if aaa=1 then
          begin
          setcolor(0);
          line(xxx,yyy,xxx,yyy-3);
          line(xxx-1,yyy-1,xxx-1,yyy-3);
          line(xxx+1,yyy-1,xxx+1,yyy-3);
          line(xxx,yyy-4,xxx,yyy-12);
          line(xxx-1,yyy-4,xxx-1,yyy-10);
          line(xxx+1,yyy-4,xxx+1,yyy-10);
          line(xxx-2,yyy-7,xxx-2,yyy-12);
          line(xxx+2,yyy-7,xxx+2,yyy-12);
          end;
          end;

PROCEDURE jeu;
          BEGIN
          aaa:=1;
          randomize;

          aaa:=1;
          ennemis1(xem1,yem1);
          ennemis1(xem2,yem2);
          ennemis1(xem3,yem3);
          ennemis1(xem4,yem4);
          ennemis1(xem5,yem5);

          if obser(xem1,yem1)=1 then  begin em1:=0; end;
          if obser(xem2,yem2)=1 then  begin em2:=0; end;
          if obser(xem3,yem3)=1 then  begin em3:=0; end;
          if obser(xem4,yem4)=1 then  begin em4:=0; end;
          if obser(xem5,yem5)=1 then  begin em5:=0; end;

          if em1=0 then begin xem1:=40+random(560); em1:=1; yem1:=-10-random(40); end;
          if em2=0 then begin xem2:=40+random(560); em2:=1; yem2:=-10-random(120); end;
          if em3=0 then begin xem3:=40+random(560); em3:=1; yem3:=-10-random(20); end;
          if em4=0 then begin xem4:=40+random(560); em4:=1; yem4:=-10-random(30); end;
          if em5=0 then begin xem5:=40+random(560); em5:=1; yem5:=-10-random(90); end;

          testx:=x;
          testy:=y;
          if a<>0 then
          begin
          b:=1;
          vaisseau;
          if ((a=1)and(x>10)) then begin x:=x-vit; end;
          if ((a=2)and(x<630)) then begin x:=x+vit; end;
          if ((a=3)and(y>10)) then begin y:=y-vit; end;
          if ((a=4)and(y<460)) then begin y:=y+vit; end;
          end;
          b:=0;

          if yem1>500 then begin em1:=0; end;
          if yem2>500 then begin em2:=0; end;
          if yem3>500 then begin em3:=0; end;
          if yem4>500 then begin em4:=0; end;
          if yem5>500 then begin em5:=0; end;

          randomize;
          if em1=1 then begin yem1:=yem1+2; if xem1<x then xem1:=xem1+1; if xem1>x then xem1:=xem1-1; end;
          if em2=1 then begin yem2:=yem2+4; if xem2<x then xem2:=xem2+1; if xem2>x then xem2:=xem2-1; end;
          if em3=1 then begin yem3:=yem3+3; if xem3<x then xem3:=xem3+1; if xem3>x then xem3:=xem3-1; end;
          if em4=1 then begin yem4:=yem4+3; if xem4<x then xem4:=xem4+1; if xem4>x then xem4:=xem4-1; end;
          if em5=1 then begin yem5:=yem5+5; if xem5<x then xem5:=xem5+1; if xem5>x then xem5:=xem5-1; end;

          aaa:=0;
          vaisseau;

          ennemis1(xem1,yem1);
          ennemis1(xem2,yem2);
          ennemis1(xem3,yem3);
          ennemis1(xem4,yem4);
          ennemis1(xem5,yem5);


          missile(m1,xm1,ym1);
          xm1:=xmm; ym1:=ymm; m1:=mm;
          missile(m2,xm2,ym2);
          xm2:=xmm; ym2:=ymm; m2:=mm;
          missile(m3,xm3,ym3);
          xm3:=xmm; ym3:=ymm; m3:=mm;
          missile(m4,xm4,ym4);
          xm4:=xmm; ym4:=ymm; m4:=mm;
          missile(m5,xm5,ym5);
          xm5:=xmm; ym5:=ymm; m5:=mm;
          missile(m6,xm6,ym6);
          xm6:=xmm; ym6:=ymm; m6:=mm;
          missile(m7,xm7,ym7);
          xm7:=xmm; ym7:=ymm; m7:=mm;
          missile(m8,xm8,ym8);
          xm8:=xmm; ym8:=ymm; m8:=mm;
          delay(4);
          if align=1 then
          begin
          align:=0;
          m1:=1;m2:=1;m3:=1;m4:=1;m5:=1;m6:=1;m7:=1;m8:=1;
          xm1:=x-2;xm2:=x-6;xm3:=x-10;xm4:=x-14;
          xm5:=x+2;xm6:=x+6;xm7:=x+10;xm8:=x+14;
          ym1:=y-8; ym2:=y-5; ym3:=y-2; ym4:=y+1;
          ym5:=y-8; ym6:=y-5; ym7:=y-2; ym8:=y+1;
          end;
          if ((testx=x) and (testy=y)) then begin a:=0; end;
           END;

PROCEDURE commande;
     VAR ch:char;
          BEGIN
               ch:=readkey;
               if ((ch=#110)and(m1=0)and(m2=0)and(m3=0)and(m4=0)and(m5=0)and(m6=0)and(m7=0)and(m8=0)) then
               begin align:=1; end;
               if (ch=#118) then
               if vit=2 then
               begin
               vit:=4;
               end
               else
               begin
               vit:=2;
               end;
               if ch=#98 then begin bouc:=1; end;
               if ch=#27 then begin closegraph; circle(0,0,0); end;
               if ch=#75 then begin a:=1; end;
               if ch=#77 then begin a:=2; end;
               if ch=#72 then begin a:=3; end;
               if ch=#80 then begin a:=4; end;
               if ch=#99 then begin a:=0; end;
               if ch=#13 then begin tps:=0; some:=df; readkey;
               fin:=chrono(df);
               df:=fin-debut;
               some:=df-some; end;
               if ch=#32 then
                  begin
                  if ((m1=1)and(m2=1)and(m3=1)and(m4=1)and(m5=1)and(m6=1)and(m7=1)and(m8=0)) then begin m8:=1; end;
                  if ((m1=1)and(m2=1)and(m3=1)and(m4=1)and(m5=1)and(m6=1)and(m7=0)) then begin m7:=1; end;
                  if ((m1=1)and(m2=1)and(m3=1)and(m4=1)and(m5=1)and(m6=0)) then begin m6:=1; end;
                  if ((m1=1)and(m2=1)and(m3=1)and(m4=1)and(m5=0)) then begin m5:=1; end;
                  if ((m1=1)and(m2=1)and(m3=1)and(m4=0)) then begin m4:=1; end;
                  if ((m1=1)and(m2=1)and(m3=0)) then begin m3:=1; end;
                  if ((m1=1)and(m2=0)) then begin m2:=1; end;
                  if m1=0 then begin m1:=1; end;
                  end;
          END;

BEGIN
     opengraph;
     logo;
     titre;
     3:scor;
     2:clearviewport;
     parametre;
     setcolor(10);
     settextstyle(10,0,3);
     outtextxy(316,100,'3');
     delay(400);
     setfillstyle(0,0);
     bar(310,95,340,150);
     outtextxy(316,100,'2');
     delay(400);
     setfillstyle(0,0);
     bar(310,95,340,150);
     outtextxy(316,100,'1');
     delay(400);
     setfillstyle(0,0);
     settextstyle(0,0,0);
     bar(310,95,340,150);
     some:=0;
     toch:=0;
     debut:=chrono(df);
     1:jeu;
     if keypressed then commande;
     if ((bouc=1)and(nbouc<>0)) then begin nbouc:=nbouc-1; bouc:=0; tps:=1; debut2:=chrono(df2); end
     else begin bouc:=0; end;
     if tps=1 then
     begin
     fin2:=chrono(df2);
     df2:=fin2-debut2;
     end;
     if df2=2 then begin tps:=0; end;
     fin:=chrono(df);
     df:=fin-debut-some;
     setfillstyle(0,0);
     bar(0,0,30,9);
     setcolor(12);
     outtextxy(0,0,chaine(df));
     if toch=1 then
     begin
     if (valeur(mots))<dff then
     begin
     recor;
     goto 3;
     end;
     goto 2;
     end;
     goto 1;
END.
