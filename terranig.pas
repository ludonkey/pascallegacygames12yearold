{$mode fpc}
PROGRAM terranig;
uses graph,crt,dos,eric,perso;
label 1;
var a,b,x,y,di,cht,aa,tt,mem,xm,ym,ym2,xm2,ht,xm3,ym3,larg,posx,posy:integer;
    chd : char;
PROCEDURE parametre;
          BEGIN
           x:=320; y:=240; di:=1; cht:=2; mem:=4;
          END;

PROCEDURE jeu(id,xt:integer);
          BEGIN
            if id<>0 then begin mem:=id; end;
           if id<>0 then
           begin
           SetFillStyle(1,0);
           bar(x,y,x+16,y+30);
           end;
           if  id=1 then
           begin
           x:=x+7;
                if xt=1 then
                begin
                droite1(x,y);
                end;
                if xt=2 then
                begin
                droite2(x,y);
                end;
           end;
           if id=2 then
              begin
              x:=x-8;
                   if xt=1 then
                   begin
                   gauche1(x,y);
                   end;
                   if xt=2 then
                   begin
                   gauche2(x,y);
                   end;
              end;
           if id=4 then
              begin
              y:=y+3;
                     if xt=1 then
                     begin
                     face1(x,y);
                     end;
                     if xt=2 then
                     begin
                     face2(x,y);
                     end;
                     end;
           if id=3 then
           begin
           y:=y-3;
                  if xt=1 then
                  begin
                  dos1(x,y);
                  end;
                  if xt=2 then
                  begin
                  dos2(x,y);
                  end;
                  end;

          END;

PROCEDURE commande;
     VAR ch:char;
          BEGIN
               ch:=readkey;
               if ch=#27 then begin closegraph; circle(0,0,0); end;
               if ch=#77 then begin di:=1; end;
               if ch=#75 then begin di:=2; end;

               if ch=#72 then begin di:=3; end;
               if ch=#80 then begin di:=4; end;
               if ch=#32 then
               begin
               if tt=1 then
                  begin
                  for b:=1 to 20 do
                      begin
                      droite2(x+b,y);
                      delay(5);
                      SetFillStyle(1,0);
                      bar(x+b-1,y,x+b+16,y+30);
                      y:=y-1;
                      end;
                  x:=x+b;
                  for b:=1 to 20 do
                      begin
                      droite2(x+b,y);
                      delay(5);
                      SetFillStyle(1,0);
                      bar(x+b-1,y,x+b+16,y+30);
                      y:=y+1;
                      end;
                  x:=x+b;
                  droite2(x,y);
               end;
               if tt=2 then
               begin
               for b:=1 to 20 do
               begin
               gauche2(x-b,y);
               delay(5);
               SetFillStyle(1,0);
               bar(x-b-1,y,x-b+16,y+30);
               y:=y-1;
               end;
               x:=x-b;
               for b:=1 to 20 do
               begin
               gauche2(x-b,y);
               delay(5);
               SetFillStyle(1,0);
               bar(x-b-1,y,x-b+16,y+30);
               y:=y+1;
               end;
               x:=x-b;
               gauche2(x,y);
               end;
               if tt=4 then
               begin
               for b:=1 to 10 do
               begin
               face1(x,y-b);
               delay(5);
               SetFillStyle(1,0);
               bar(x-1,y-b,x+16,y-b+30);
               end;
               y:=y-b;
               for b:=1 to 40 do
               begin
               face2(x,y+b);
               delay(5);
               SetFillStyle(1,0);
               bar(x-1,y+b,x+16,y+30+b);
               end;
               y:=y+b;
               face2(x,y);
               end;
               if tt=3 then
               begin
               for b:=1 to 40 do
               begin
               dos1(x,y-b);
               delay(5);
               SetFillStyle(1,0);
               bar(x-1,y-b,x+16,y-b+30);
               end;
               y:=y-b;
               for b:=1 to 10 do
               begin
               dos2(x,y+b);
               delay(5);
               SetFillStyle(1,0);
               bar(x-1,y+b,x+16,y+30+b);
               end;
               y:=y+b;
               dos2(x,y);
               end;
               end;
               if ch=#13 then
               begin
               if mem=4 then
               begin
               xm:=-49;
               ym:=35;
               xm2:=0;
               ym2:=70;
               xm3:=49;
               ym3:=0;
               ht:=60;
               larg:=0;
               end;
               if mem=3 then
               begin
               xm:=-49;
               ym:=-35;
               xm2:=0;
               ym2:=0;
               xm3:=49;
               ym3:=-70;
               ht:=-11;
               larg:=0;
               end;
               if mem=1 then
               begin
               xm:=0;
               ym:=0;
               xm2:=49;
               ym2:=35;
               xm3:=98;
               ym3:=-35;
               ht:=22;
               larg:=49;
               end;
               if mem=2 then
               begin
               xm:=-98;
               ym:=0;
               xm2:=-49;
               ym2:=35;
               xm3:=0;
               ym3:=-35;
               ht:=22;
               larg:=-49;
               end;
               if mem<>1 then begin droite1(x+xm,y+ym); delay(400); end;
               if mem<>3 then begin dos1(x+xm2,y+ym2); delay(400); end;
               if mem<>2 then begin gauche1(x+xm3,y+ym); delay(400); end;
               if mem<>4 then begin face1(x+xm2,y+ym3); delay(400); end;
               setcolor(4);
               aa:=-60;
               while aa<y+ht do
               begin
               aa:=aa+3;
               ellipse(x+7+larg,aa,0,360,57,40);
               delay(1);
               end;
               while aa>-60 do
               begin
               aa:=aa-3;
               ellipse(x+7+larg,aa,0,360,57,40);
               delay(1);
               end;
               while aa<y+ht do
               begin
               aa:=aa+1;
               ellipse(x+7+larg,aa,0,360,57,40);
               delay(1);
               end;
               aa:=aa+2;
               while aa>-60 do
               begin
               if mem=1 then begin droite1(x,y); end;
               if mem=2 then begin gauche1(x,y); end;
               if mem=3 then begin dos1(x,y); end;
               if mem=4 then begin face1(x,y); end;
               setcolor(0);
               cht:=2;
               di:=4;
               ellipse(x+7+larg,aa,180,360,57,40);
               aa:=aa-1;
               delay(1);
               end;
               end;
               END;

BEGIN
     opengraph;
     parametre;
     di:=4;
     1:jeu(di,cht);
     if keypressed then
     begin

     posx:=x;
     posy:=y;
            commande;
            commande;
            commande;
             tt:=di;
      if cht=1 then
      begin
      cht:=2;
      end
      else
      begin
      cht:=1;
      end;

            end else begin di:=0; end;




     goto 1;
END.
