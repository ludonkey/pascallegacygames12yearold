{$mode fpc}
PROGRAM jeux;
uses graph,crt,dos,eric;
label 1,2,3,4,5,6;
var a2,b2,c2,d2,x,y,a,b,c,d,color,color2,haut,mur,sth,stm,choix,tp,x1,y1,a1,b1,c1,d1,a3,b3,c3,d3,x2,y2,a4,b4,c4,d4,tp1,tx1,ty1,
    tx2,ty2,es,es2,si1,si2,si3,si4,test,test1,sit1,sit2,sit3,sit4,est,testt,test1t,dur,pouv,chron,score,sauf,et,point,tppouv,
    vie,time,col1,col2,col3,col4,po1,po2,po3,po4,px1,py1,pt1,px2,py2,pt2,px3,py3,pt3,px4,py4,pt4,px5,pt5,px6,pt6,px7,
    pt7,px8,pt8,px9,pt9,px10,pt10,px11,pt11,px12,pt12,px13,pt13,px14,pt14,px15,pt15,
    px16,pt16,px17,pt17,px18,pt18,px19,pt19,px20,pt20,px21,pt21,px22,pt22,px23,pt23,
    px24,pt24,px25,pt25,px26,pt26,px27,pt27,py5,py6,py7,yyy,calc,zzz,xxx,
    px28,pt28,px29,pt29,px30,pt30,pt41,pt42,zzzz,aaa,bbb,zy,let1,let2,let3,let4,let5,let6,
    px31,pt31,px32,pt32,px33,pt33,px34,pt34,azaz,azer,xx2,eu,xxx2,dex,dey,
    px35,pt35,px36,pt36,px37,pt37,px41,px42,sort,cht,chtt,esx,esy,dea,deb,dec,ded,
    px38,pt38,px39,pt39,px40,pt40,abcd,gagner,cp:integer;

PROCEDURE parametre;
          BEGIN
          x:=315; y:=415; a:=1; b:=0; c:=0; d:=0; color:=14; haut:=7; mur:=3; sth:=7; stm:=9; pouv:=0;
          tp:=0;  x1:=65; y1:=85; a1:=0; b1:=0; c1:=0; d1:=0; a2:=1; b2:=0; c2:=0; d2:=0; color2:=8;
          x2:=575; y2:=85; a3:=0; b3:=0; c3:=0; d3:=0; a4:=1; b4:=0; c4:=0; d4:=0; tp1:=0; choix:=0;
          tx1:=0; ty1:=0; tx2:=0; ty2:=0; es:=0; si1:=0; si2:=0; si3:=0; si4:=0; test:=0; test1:=0;
          score:=0; a2:=0;b2:=0; c2:=0; d2:=0;  es2:=0; sit1:=0;sit2:=0;sit3:=0;sit4:=0;est:=0;
          testt:=0; test1t:=0; tppouv:=0; vie:=3; time:=0; col1:=10; col2:=10; col3:=10; col4:=10;
          po1:=0; po2:=0; po3:=0; po4:=0; dur:=3; chron:=0; sauf:=0; et:=0; point:=0;
          px1:=95; py1:=65; pt1:=0;  px2:=315;  pt2:=0; px6:=475;  pt6:=0; calc:=0;
          px3:=545;  pt3:=0; px4:=165;  pt4:=0; px5:=245;  pt5:=0; pt41:=0; pt42:=0; esy:=0;
          px7:=395;  pt7:=0; px8:=95; py2:=415; pt8:=0; px9:=475;  pt9:=0; px10:=545;  pt10:=0;
          px11:=165;  pt11:=0; px12:=245;  pt12:=0; px13:=395;  pt13:=0; yyy:=0; zzz:=0; xxx:=0;
          py3:=345; px14:=65; pt14:=0; px15:=315;  pt15:=0; px16:=575;  pt16:=0; px17:=165;
          pt17:=0; px18:=245;  pt18:=0; px19:=475;  pt19:=0; px20:=395;  pt20:=0; yyy:=0;
          px21:=395; py4:=135; pt21:=0; px22:=65;  pt22:=0; px23:=575;  pt23:=0; px24:=165;
          pt24:=0; px25:=245;  pt25:=0; px26:=475;  pt26:=0; py5:=285; py6:=195; py7:=245;
          px27:=65; pt27:=0; px28:=315;  pt28:=0; px29:=575;  pt29:=0; px30:=135; px41:=275;
          pt30:=0; px31:=245;  pt31:=0; px32:=505;  pt32:=0; px33:=395;  pt33:=0; px42:=355;
          px34:=65; pt34:=0; px35:=315;  pt35:=0; px36:=575;  pt36:=0; px37:=135; cht:=0;
          pt37:=0; px38:=245;  pt38:=0; px39:=505;  pt39:=0; px40:=395;  pt40:=0; chtt:=0;
          zzzz:=0; azaz:=1; azer:=0; zy:=0; eu:=0; xx2:=eu-50; xxx2:=eu-300; sort:=0; esx:=0;
          dex:=x; dey:=y; dea:=1; deb:=0; dec:=0; ded:=0; abcd:=0; gagner:=0; cp:=4;
          END;

PROCEDURE Commande3;
          VAR ch:char;
          BEGIN
          ch:=readkey;
          if(ch=#59)and(sort=0)then begin sort:=2; end;
          END;

PROCEDURE logo;
          BEGIN
          setcolor(1);
          line(470,128,350,128);line(350,128,505,163);line(470,128,505,163);
          line(475,126,510,20); line(510,20,510,161); line(475,126,510,161);
          line(515,115,515,170); line(515,115,580,250); line(515,170,580,250);
          arc2(373,392,90,270,16); arc2(530,392,270,90,16);
          line(373,376,530,376);  line(373,408,530,408);
          TEXTCOLOR (1);
          setcolor(1);
          settextstyle(10,0,7);
          outtextxy(100,150,'TAKAMI');
          settextstyle(1,0,4);
          outtextxy(370,370,'Production');
          setfillstyle(2,1);
          floodfill(0,0,1);
          floodfill(195,235,1);
          floodfill(335,235,1);
          delay(3000);
          clearviewport;
          setcolor(15);
          settextstyle(10,0,3);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(7);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(8);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(0);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          let1:=120; let2:=100; let3:=100; let4:=100;
          let5:=100; let6:=100;
         END;

PROCEDURE Titre;
          BEGIN
          setcolor(14);
          settextstyle(10,0,8);
          outtextxy(let1,100,'P');
          delay(200);
          outtextxy(let2,100,'A');
          delay(200);
          outtextxy(let3,100,'C');
          delay(200);
          outtextxy(let4,100,'M');
          delay(200);
          outtextxy(let5,100,'A');
          delay(200);
          outtextxy(let6,100,'N');
          delay(200);
          while (let6<>500)and(sort<>2) do
          begin

          if let2<>180 then begin let2:=let2+1; end;
          if let3<>260 then begin let3:=let3+1; end;
          if let4<>340 then begin let4:=let4+1; end;
          if let5<>420 then begin let5:=let5+1; end;
          if let6<>500 then begin let6:=let6+1; end;
          setcolor(0);
          outtextxy(let6-1,100,'N');
          setcolor(14);
          outtextxy(let6,100,'N');
          setcolor(0);
          outtextxy(let5-1,100,'A');
          setcolor(14);
          outtextxy(let5,100,'A');
          setcolor(0);
          outtextxy(let4-1,100,'M');
          setcolor(14);
          outtextxy(let4,100,'M');
          setcolor(0);
          outtextxy(let3-1,100,'C');
          setcolor(14);
          outtextxy(let3,100,'C');
          setcolor(0);
          outtextxy(let2-1,100,'A');
          setcolor(14);
          outtextxy(let2,100,'A');
          setcolor(0);
          outtextxy(let1-1,100,'P');
          setcolor(14);
          outtextxy(let1,100,'P');
          end;
          setcolor(10);
          settextstyle(0,0,1);
          outtextxy(160,50,'Pour passer l''intro, presser la touche F1.');
          setfillstyle(1,10);
          setcolor(10);
          bar(0,375,649,500);
          setfillstyle(1,8);
          setcolor(8);
          bar(0,375,649,382);
          setfillstyle(1,2);
          setcolor(2);
          bar(0,382,649,382);
          bar(0,392,649,407);
          bar(0,427,649,452);
          SetLineStyle(DottedLn+2, 20, NormWidth);
          setcolor(15);
          line(0,377,649,377);
          while (xx2<700)and(sort<>2) do
          begin
          if keypressed then commande3;
          xx2:=xx2+1;
          xxx2:=xxx2+2;
          if xx2=200 then begin
          settextstyle(20,0,1);
          setcolor(15);
          outtextxy(xx2-10,330,'PACMAN');
          delay(1000);
          setcolor(0);
          outtextxy(xx2-10,330,'PACMAN');
          abcd:=1;
          end;
          if (xxx2-700=200)and(abcd=1) then begin
          settextstyle(20,0,1);
          setcolor(15);
          outtextxy(xxx2-760,330,'Le fantome de pacman');
          delay(1000);
          setcolor(0);
          outtextxy(xxx2-760,330,'Le fantome de pacman');
          end;
          setfillstyle(1,14);
          setcolor(14);
          PieSlice(xx2,360,60,300,13);
          PieSlice(xx2,360,300,355,7);
          PieSlice(xx2,360,5,60,7);
          PieSlice(xx2,360,0,5,12);
          PieSlice(xx2,360,359,360,12);
          arc2(xx2,360,0,60,13);
          setcolor(0);
          arc2(xx2-1,360,7,59,13);
            arc2(xx2,360,90,270,14);
               arc2(xx2-1,360,90,270,14);
               arc2(xx2-2,360,90,270,14);
               arc2(xx2-3,360,90,270,14);
               arc2(xx2-4,360,90,270,13);
               arc2(xx2-5,360,90,270,13);
               arc2(xx2-6,360,90,270,13);
               arc2(xx2-7,360,90,270,13);
               arc2(xx2-8,360,90,270,13);
               arc2(xx2-9,360,90,270,13);
               arc2(xx2-10,360,90,270,13);
               arc2(xx2-11,360,90,270,13);
               arc2(xx2-12,360,90,140,13);
               arc2(xx2-12,360,220,270,13);
          setfillstyle(1,8);
          setcolor(8);
          PieSlice(xxx2-700,360,60,300,13);
          PieSlice(xxx2-700,360,300,355,7);
          PieSlice(xxx2-700,360,5,60,7);
          PieSlice(xxx2-700,360,0,5,12);
          PieSlice(xxx2-700,360,359,360,12);
          arc2(xxx2-700,360,0,60,13);
          setcolor(0);
          arc2(xxx2-702,360,7,59,13);
            arc2(xx2-700,360,90,270,14);
               arc2(xxx2-701,360,90,270,14);
               arc2(xxx2-702,360,90,270,14);
               arc2(xxx2-703,360,90,270,14);
               arc2(xxx2-704,360,90,270,13);
               arc2(xxx2-705,360,90,270,13);
               arc2(xxx2-706,360,90,270,13);
               arc2(xxx2-707,360,90,270,13);
               arc2(xxx2-708,360,90,270,13);
               arc2(xxx2-709,360,90,270,13);
               arc2(xxx2-710,360,90,270,13);
               arc2(xxx2-711,360,90,270,13);
               arc2(xxx2-712,360,90,140,13);
               arc2(xxx2-712,360,220,270,13);

          end;
          abcd:=0;
          while (xx2>eu-200)and(sort<>2) do
          begin
          if xxx2-700=400  then begin
          settextstyle(20,0,1);
          setcolor(15);
          outtextxy(xxx2-660,330,'Fantome en fuite !');
          delay(1000);
          setcolor(0);
          outtextxy(xxx2-660,330,'Fantome en fuite !');
          abcd:=1;
          end;
          if (xx2=150)and(abcd=1) then begin
          settextstyle(20,0,1);
          setcolor(15);
          outtextxy(450,330,'Pacman GLOUTON !');
          delay(1000);
          setcolor(0);
          outtextxy(450,330,'Pacman GLOUTON !');
          end;
          if keypressed then commande3;
          xx2:=xx2-1;
          xxx2:=xxx2-2;
          setfillstyle(1,14);
          setcolor(14);
          PieSlice(xxx2+200,360,0,120,13);
          PieSlice(xxx2+200,360,240,360,13);
          PieSlice(xxx2+200,360,185,240,7);
          PieSlice(xxx2+200,360,120,175,7);
          PieSlice(xxx2+200,360,175,185,13);
          arc2(xxx2+200,360,120,180,13);
           setcolor(0);
          arc2(xxx2+202,360,120,180,13);
               arc2(xxx2+200,360,270,90,14);
               arc2(xxx2+201,360,270,90,14);
               arc2(xxx2+202,360,270,90,14);
               arc2(xxx2+203,360,270,90,14);
               arc2(xxx2+204,360,270,90,13);
               arc2(xxx2+205,360,270,90,13);
               arc2(xxx2+206,360,270,90,13);
               arc2(xxx2+207,360,270,90,13);
               arc2(xxx2+208,360,270,90,13);
               arc2(xxx2+209,360,270,90,13);
               arc2(xxx2+210,360,270,90,13);
               arc2(xxx2+211,360,270,90,13);
               arc2(xxx2+212,360,270,320,13);
               arc2(xxx2+212,360,40,90,13);
          setfillstyle(1,13);
          setcolor(13);
          PieSlice(xx2,360,0,120,13);
          PieSlice(xx2,360,240,360,13);
          PieSlice(xx2,360,185,240,7);
          PieSlice(xx2,360,120,175,7);
          PieSlice(xx2,360,175,185,13);
          arc2(xx2,360,120,180,13);
          setcolor(0);
               arc2(xx2+1,360,121,179,13);
               arc2(xx2,360,270,90,14);
               arc2(xx2+1,360,270,90,14);
               arc2(xx2+2,360,270,90,14);
               arc2(xx2+3,360,270,90,14);
               arc2(xx2+4,360,270,90,13);
               arc2(xx2+5,360,270,90,13);
               arc2(xx2+6,360,270,90,13);
               arc2(xx2+7,360,270,90,13);
               arc2(xx2+8,360,270,90,13);
               arc2(xx2+9,360,270,90,13);
               arc2(xx2+10,360,270,90,13);
               arc2(xx2+11,360,270,90,13);
               arc2(xx2+12,360,270,320,13);
               arc2(xx2+12,360,40,90,13);
               end;
               while(xx2<320)and(sort<>2) do
          begin
          if keypressed then commande3;
          xx2:=xx2+1;
          setfillstyle(1,14);
          setcolor(14);
          PieSlice(xx2,360,60,300,13);
          PieSlice(xx2,360,300,355,7);
          PieSlice(xx2,360,5,60,7);
          PieSlice(xx2,360,0,5,12);
          PieSlice(xx2,360,359,360,12);
          arc2(xx2,360,0,60,13);
          setcolor(0);
          arc2(xx2-1,360,7,59,13);
            arc2(xx2,360,90,270,14);
               arc2(xx2-1,360,90,270,14);
               arc2(xx2-2,360,90,270,14);
               arc2(xx2-3,360,90,270,14);
               arc2(xx2-4,360,90,270,13);
               arc2(xx2-5,360,90,270,13);
               arc2(xx2-6,360,90,270,13);
               arc2(xx2-7,360,90,270,13);
               arc2(xx2-8,360,90,270,13);
               arc2(xx2-9,360,90,270,13);
               arc2(xx2-10,360,90,270,13);
               arc2(xx2-11,360,90,270,13);
               arc2(xx2-12,360,90,140,13);
               arc2(xx2-12,360,220,270,13);
             end;
          if sort<>2 then
          begin
          setfillstyle(1,14);
          setcolor(14);
          PieSlice(xx2,360,0,360,13);
          setfillstyle(1,0);
          setcolor(0);
          PieSlice(xx2-5,358,0,360,3);
          PieSlice(xx2+5,358,0,360,3);
          pieslice(xx2,362,180,360,9);
          setcolor(14);
          settextstyle(0,0,1);
          outtextxy(xx2-35,335,'MIAM!');
          delay(700);
          outtextxy(xx2+10,335,'MIAM!');
          delay(5000);
          end;
          END;

PROCEDURE menu;
          begin
          settextstyle(1,0,7);  (*fontes,orientation,taille*)
          setcolor(11);
          outtextxy(20,60,'********MENU********');
          outtextxy(17,373,'***********************');
          settextstyle(1,1,7);  (*fontes,orientation,taille*)
          setcolor(12);
          outtextxy(25,50,'***************');
          outtextxy(535,50,'***************');
          settextstyle(1,0,4);  (*fontes,orientation,taille*)
          setcolor(15);
          if zzzz=1 then
          begin
          outtextxy(260,180,'REPRENDRE');
          outtextxy(260,250,'NOUVEAU JEU');
          outtextxy(260,320,'SORTIR');
          end;
          if zzzz=0 then
          begin
          outtextxy(260,200,'NOUVEAU JEU');
          outtextxy(260,270,'SORTIR');
          end;
          end;

PROCEDURE entre;
          begin
          setfillstyle(1,14);
          setcolor(14);
          PieSlice(aaa,bbb,60,300,27);
          PieSlice(aaa,bbb,300,358,13);
          PieSlice(aaa,bbb,5,60,13);
          PieSlice(aaa,bbb,0,5,26);
          PieSlice(aaa,bbb,359,360,26);
          arc2(aaa,bbb,0,60,27);

          end;

PROCEDURE commande2;
            VAR ch:char;
          BEGIN
          ch:=readkey;
                      if zzzz=0 then
                      begin
                           if (ch=#72)and(azaz=2)then
                           begin
                           setfillstyle(1,0);
          setcolor(0);
          PieSlice(aaa,bbb,60,300,27);
          PieSlice(aaa,bbb,300,358,13);
          PieSlice(aaa,bbb,5,60,13);
          PieSlice(aaa,bbb,0,5,26);
          PieSlice(aaa,bbb,359,360,26);
          arc2(aaa,bbb,0,60,27);
                           azaz:=1;
                           bbb:=bbb-70;
                           end;
                           if (ch=#80)and(azaz=1)then
                           begin
                                            setfillstyle(1,0);
          setcolor(0);
          PieSlice(aaa,bbb,60,300,27);
          PieSlice(aaa,bbb,300,358,13);
          PieSlice(aaa,bbb,5,60,13);
          PieSlice(aaa,bbb,0,5,26);
          PieSlice(aaa,bbb,359,360,26);
          arc2(aaa,bbb,0,60,27);
                           azaz:=2;
                           bbb:=bbb+70;
                           end;
                      end;
                      if zzzz=1 then
                         begin
                         if (ch=#72)and(azaz<>0)then
                         begin
                                          setfillstyle(1,0);
          setcolor(0);
          PieSlice(aaa,bbb,60,300,27);
          PieSlice(aaa,bbb,300,358,13);
          PieSlice(aaa,bbb,5,60,13);
          PieSlice(aaa,bbb,0,5,26);
          PieSlice(aaa,bbb,359,360,26);
          arc2(aaa,bbb,0,60,27);
                         azaz:=azaz-1;
                         bbb:=bbb-70;
                         end;
                         if (ch=#80)and(azaz<>2)then
                         begin
                                          setfillstyle(1,0);
          setcolor(0);
          PieSlice(aaa,bbb,60,300,27);
          PieSlice(aaa,bbb,300,358,13);
          PieSlice(aaa,bbb,5,60,13);
          PieSlice(aaa,bbb,0,5,26);
          PieSlice(aaa,bbb,359,360,26);
          arc2(aaa,bbb,0,60,27);
                         azaz:=azaz+1;
                         bbb:=bbb+70;
                         end;
                      end;
          if ch=#13 then
          begin
          while aaa<460 do
          begin
             aaa:=aaa+3;
             setcolor(14);
             arc2(aaa,bbb,0,60,27);
              setcolor(0);
              arc2(aaa-3,bbb,7,59,27);
               setcolor(14);
               PieSlice(aaa,bbb,60,300,27);
               PieSlice(aaa,bbb,300,358,13);
               PieSlice(aaa,bbb,5,60,13);
               PieSlice(aaa,bbb,0,5,28);
               PieSlice(aaa,bbb,359,360,26);
               setcolor(0);
               arc2(aaa,bbb,90,270,28);
               arc2(aaa-1,bbb,90,270,28);
               arc2(aaa-2,bbb,90,270,28);
               arc2(aaa-3,bbb,90,270,28);
               arc2(aaa-4,bbb,90,270,27);
               arc2(aaa-5,bbb,90,270,27);
               arc2(aaa-6,bbb,90,270,27);
               arc2(aaa-7,bbb,90,270,27);
               arc2(aaa-8,bbb,90,270,27);
               arc2(aaa-9,bbb,90,270,27);
               arc2(aaa-10,bbb,90,270,27);
               arc2(aaa-11,bbb,90,270,27);
               arc2(aaa-12,bbb,90,140,27);
               arc2(aaa-12,bbb,220,270,27);
                 azer:=1;
               end;
                 delay(500);
          end;

          end;

PROCEDURE Gagne;
          BEGIN
          setfillstyle(4,8);
          bar(180,140,460,340);
          setfillstyle(1,2);
          bar(190,150,450,330);
          setfillstyle(1,12);
          bar(200,160,440,320);
          setfillstyle(1,14);
          setcolor(14);
          PieSlice(240,240,0,360,8);
          setcolor(10);
          line(260,245,270,235);
          line(260,235,270,245);
          line(260,285,270,295);
          line(260,295,270,285);
          settextstyle(3,0,1);
          if point=42 then
          begin
          outtextxy(240,170,('VOUS AVEZ GAGNE'));
          end;
          if vie<0 then
          begin
          outtextxy(240,170,('VOUS AVEZ PERDU'));
          end;
          outtextxy(270,250,('+'));
          rectangle(230,170,410,195);
          outtextxy(210,200,('SCORE:'));
          line(210,223,267,223);
          settextstyle(3,0,1);
          setcolor(15);

          outtextxy(280,227,(chaine(point)));
          outtextxy(280,277,(chaine(calc)));
          setfillstyle(1,0);
          bar(320,280,420,300);
          setfillstyle(1,8);
          setcolor(8);
          PieSlice(240,290,60,300,13);
          PieSlice(240,290,300,355,7);
          PieSlice(240,290,5,60,7);
          PieSlice(240,290,0,5,12);
          PieSlice(240,290,359,360,12);
          arc2(240,290,0,60,13);
          for yyy:=0 to point*25 do
           begin
          setfillstyle(1,0);
          bar(320,230,420,250);
          setcolor(13);
          outtextxy(330,227,(chaine(yyy)));
          xxx:=yyy;

           end;
          for zzz:=0 to calc*100 do
           begin
          setfillstyle(1,0);
          bar(320,280,420,300);
          setcolor(13);
          outtextxy(330,277,(chaine(xxx+zzz)));

           end;
           settextstyle(10,0,2);

           sort:=0;
          while sort=0 do
          begin
          if cp=12 then begin cp:=4; end else cp:=12;
          setcolor(cp);
          outtextxy(230,38,('PRESSEZ F1'));
          if keypressed then commande3;
          if sort=0 then
          begin
          delay(300);
          end;
          end;
          zy:=1;
          gagner:=1;

          END;
PROCEDURE Decor;
          BEGIN
          setfillstyle(sth,haut);
          setcolor(sth);
          bar(0,0,640,480);
          setfillstyle(stm,mur);
          setcolor(6);
          bar(20,20,620,458);
          setfillstyle(1,0);
          setcolor(0);
          bar(50,50,590,430);
          setfillstyle(sth,haut);
          setcolor(haut);
          bar(80,80,560,400);
          setfillstyle(stm,mur);
          setcolor(mur);
          bar(100,100,540,380);
          setfillstyle(1,0);
          setcolor(0);
          bar(120,120,520,360);
          setfillstyle(sth,haut);
          setcolor(haut);
          bar(150,150,490,330);
          setfillstyle(stm,mur);
          setcolor(mur);
          bar(170,170,470,310);
          setfillstyle(1,0);
          setcolor(0);
          bar(180,180,460,300);
          setfillstyle(sth,haut);
          setcolor(haut);
          bar(210,210,430,270);
          setfillstyle(stm,mur);
          setcolor(mur);
          bar(220,220,420,265);
          setfillstyle(1,0);
          setcolor(0);
          bar(230,230,410,260);
          setcolor(15);
          rectangle(0,0,639,479);
          rectangle(20,20,619,459);
          rectangle(50,50,590,430);
          rectangle(80,80,560,400);
          rectangle(100,100,540,380);
          rectangle(120,120,520,360);
          rectangle(150,150,490,330);
          rectangle(170,170,470,310);
          rectangle(180,180,460,300);
          rectangle(210,210,430,270);
          rectangle(220,220,420,265);
          line(220,265,230,260);
          line(410,260,420,265);
          line(230,260,410,260);
          line(470,310,460,300);
          line(540,380,520,360);
          line(470,170,460,180);
          line(540,100,520,120);
          line(170,310,180,300);
          line(100,380,120,360);
          line(20,20,50,50);
          line(619,459,589,429);
          line(619,20,589,50);
          line(20,459,50,429);
          line(100,100,120,120);
          line(170,170,180,180);
          line(230,230,410,230);
          line(230,230,230,260);
          line(410,230,410,260);
          line(220,220,230,230);
          line(420,220,410,230);
          setfillstyle(1,0);
          setcolor(0);
          bar(300,75,330,125);
          bar(300,355,330,405);
          bar(455,230,495,260);
          bar(145,230,185,260);
          bar(300,205,330,235);
          setcolor(15);
          line(300,80,300,120);
          line(330,80,330,120);
          line(300,360,300,400);
          line(330,360,330,400);
          line(460,230,490,230);
          line(460,260,490,260);
          line(150,230,180,230);
          line(150,260,180,260);
          line(300,210,300,230);
          line(330,210,330,230);
          setfillstyle(1,0);
          setcolor(0);
          bar(21,5,90,15);
          settextstyle(0,0,1);
          setcolor(12);
          outtextxy(30,8,'Pouvoir:');
          setfillstyle(1,0);
          setcolor(0);
          bar(149,5,259,15);
          setcolor(12);
          outtextxy(160,8,'Phantom:');
          setfillstyle(1,0);
          setcolor(0);
          bar(269,5,379,15);
          setcolor(12);
          outtextxy(280,8,'Vie:');
          END;
PROCEDURE jeu;
          BEGIN

          setfillstyle(1,color);
          setcolor(color);

          if (px1=x)and(py1=y)then begin pt1:=1; end;
          if pt1=0 then
          begin
          PieSlice(px1,py1+5,0,360,5);
          end;
          if pt1=1 then
          begin
          px1:=800;
          point:=point+1;
          pt1:=2;
          end;
          if (px2=x)and(py1=y)then begin pt2:=1; end;
          if pt2=0 then
          begin
          PieSlice(px2-5,py1+5,0,360,5);
          end;
          if pt2=1 then
          begin
          px2:=800;
          point:=point+1;
          pt2:=2;
          end;
          if (px3=x)and(py1=y)then begin pt3:=1; end;
          if pt3=0 then
          begin
          PieSlice(px3,py1+5,0,360,5);
          end;
          if pt3=1 then
          begin
          px3:=800;
          point:=point+1;
          pt3:=2;
          end;
          if (px4=x)and(py1=y)then begin pt4:=1; end;
          if pt4=0 then
          begin
          PieSlice(px4,py1+5,0,360,5);
          end;
          if pt4=1 then
          begin
          px4:=800;
          point:=point+1;
          pt4:=2;
          end;
          if (px5=x)and(py1=y)then begin pt5:=1; end;
          if pt5=0 then
          begin
          PieSlice(px5,py1+5,0,360,5);
          end;
          if pt5=1 then
          begin
          px5:=800;
          point:=point+1;
          pt5:=2;
          end;
          if (px6=x)and(py1=y)then begin pt6:=1; end;
          if pt6=0 then
          begin
          PieSlice(px6,py1+5,0,360,5);
          end;
          if pt6=1 then
          begin
          px6:=800;
          point:=point+1;
          pt6:=2;
          end;
          if (px7=x)and(py1=y)then begin pt7:=1; end;
          if pt7=0 then
          begin
          PieSlice(px7,py1+5,0,360,5);
          end;
          if pt7=1 then
          begin
          px7:=800;
          point:=point+1;
          pt7:=2;
          end;
          if (px8=x)and(py2=y)then begin pt8:=1; end;
          if pt8=0 then
          begin
          PieSlice(px8,py2+5,0,360,5);
          end;
          if pt8=1 then
          begin
          px8:=800;
          point:=point+1;
          pt8:=2;
          end;
          if (px9=x)and(py2=y)then begin pt9:=1; end;
          if pt9=0 then
          begin
          PieSlice(px9,py2+5,0,360,5);
          end;
          if pt9=1 then
          begin
          px9:=800;
          point:=point+1;
          pt9:=2;
          end;
          if (px10=x)and(py2=y)then begin pt10:=1; end;
          if pt10=0 then
          begin
          PieSlice(px10,py2+5,0,360,5);
          end;
          if pt10=1 then
          begin
          px10:=800;
          point:=point+1;
          pt10:=2;
          end;
          if (px11=x)and(py2=y)then begin pt11:=1; end;
          if pt11=0 then
          begin
          PieSlice(px11,py2+5,0,360,5);
          end;
          if pt11=1 then
          begin
          px11:=800;
          point:=point+1;
          pt11:=2;
          end;
          if (px12=x)and(py2=y)then begin pt12:=1; end;
          if pt12=0 then
          begin
          PieSlice(px12,py2+5,0,360,5);
          end;
          if pt12=1 then
          begin
          px12:=800;
          point:=point+1;
          pt12:=2;
          end;
          if (px13=x)and(py2=y)then begin pt13:=1; end;
          if pt13=0 then
          begin
          PieSlice(px13,py2+5,0,360,5);
          end;
          if pt13=1 then
          begin
          px13:=800;
          point:=point+1;
          pt13:=2;
          end;
          if (px14=x)and(py3=y)then begin pt14:=1; end;
          if pt14=0 then
          begin
          PieSlice(px14-5,py3+5,0,360,5);
          end;
          if pt14=1 then
          begin
          px14:=800;
          point:=point+1;
          pt14:=2;
          end;
          if (px15=x)and(py3=y)then begin pt15:=1; end;
          if pt15=0 then
          begin
          PieSlice(px15-5,py3+5,0,360,5);
          end;
          if pt15=1 then
          begin
          px15:=800;
          point:=point+1;
          pt15:=2;
          end;
          if (px16=x)and(py3=y)then begin pt16:=1; end;
          if pt16=0 then
          begin
          PieSlice(px16-5,py3+5,0,360,5);
          end;
          if pt16=1 then
          begin
          px16:=800;
          point:=point+1;
          pt16:=2;
          end;
          if (px17=x)and(py3=y)then begin pt17:=1; end;
          if pt17=0 then
          begin
          PieSlice(px17,py3+5,0,360,5);
          end;
          if pt17=1 then
          begin
          px17:=800;
          point:=point+1;
          pt17:=2;
          end;
          if (px18=x)and(py3=y)then begin pt18:=1; end;
          if pt18=0 then
          begin
          PieSlice(px18,py3+5,0,360,5);
          end;
          if pt18=1 then
          begin
          px18:=800;
          point:=point+1;
          pt18:=2;
          end;
          if (px19=x)and(py3=y)then begin pt19:=1; end;
          if pt19=0 then
          begin
          PieSlice(px19,py3+5,0,360,5);
          end;
          if pt19=1 then
          begin
          px19:=800;
          point:=point+1;
          pt19:=2;
          end;
          if (px20=x)and(py3=y)then begin pt20:=1; end;
          if pt20=0 then
          begin
          PieSlice(px20,py3+5,0,360,5);
          end;
          if pt20=1 then
          begin
          px20:=800;
          point:=point+1;
          pt20:=2;
          end;
          if (px21=x)and(py4=y)then begin pt21:=1; end;
          if pt21=0 then
          begin
          PieSlice(px21,py4+5,0,360,5);
          end;
          if pt21=1 then
          begin
          px21:=800;
          point:=point+1;
          pt21:=2;
          end;
          if (px22=x)and(py4=y)then begin pt22:=1; end;
          if pt22=0 then
          begin
          PieSlice(px22-5,py4+5,0,360,5);
          end;
          if pt22=1 then
          begin
          px22:=800;
          point:=point+1;
          pt22:=2;
          end;
          if (px23=x)and(py4=y)then begin pt23:=1; end;
          if pt23=0 then
          begin
          PieSlice(px23-5,py4+5,0,360,5);
          end;
          if pt23=1 then
          begin
          px23:=800;
          point:=point+1;
          pt23:=2;
          end;
          if (px24=x)and(py4=y)then begin pt24:=1; end;
          if pt24=0 then
          begin
          PieSlice(px24,py4+5,0,360,5);
          end;
          if pt24=1 then
          begin
          px24:=800;
          point:=point+1;
          pt24:=2;
          end;
          if (px25=x)and(py4=y)then begin pt25:=1; end;
          if pt25=0 then
          begin
          PieSlice(px25,py4+5,0,360,5);
          end;
          if pt25=1 then
          begin
          px25:=800;
          point:=point+1;
          pt25:=2;
          end;
          if (px26=x)and(py4=y)then begin pt26:=1; end;
          if pt26=0 then
          begin
          PieSlice(px26,py4+5,0,360,5);
          end;
          if pt26=1 then
          begin
          px26:=800;
          point:=point+1;
          pt26:=2;
          end;
          if (px27=x)and(py5=y)then begin pt27:=1; end;
          if pt27=0 then
          begin
          PieSlice(px27-5,py5+5,0,360,5);
          end;
          if pt27=1 then
          begin
          px27:=800;
          point:=point+1;
          pt27:=2;
          end;
          if (px28=x)and(py5=y)then begin pt28:=1; end;
          if pt28=0 then
          begin
          PieSlice(px28-5,py5+5,0,360,5);
          end;
          if pt28=1 then
          begin
          px28:=800;
          point:=point+1;
          pt28:=2;
          end;
          if (px29=x)and(py5=y)then begin pt29:=1; end;
          if pt29=0 then
          begin
          PieSlice(px29-5,py5+5,0,360,5);
          end;
          if pt29=1 then
          begin
          px29:=800;
          point:=point+1;
          pt29:=2;
          end;
          if (px30=x)and(py5=y)then begin pt30:=1; end;
          if pt30=0 then
          begin
          PieSlice(px30-5,py5+5,0,360,5);
          end;
          if pt30=1 then
          begin
          px30:=800;
          point:=point+1;
          pt30:=2;
          end;
          if (px31=x)and(py5=y)then begin pt31:=1; end;
          if pt31=0 then
          begin
          PieSlice(px31,py5+5,0,360,5);
          end;
          if pt31=1 then
          begin
          px31:=800;
          point:=point+1;
          pt31:=2;
          end;
          if (px32=x)and(py5=y)then begin pt32:=1; end;
          if pt32=0 then
          begin
          PieSlice(px32-5,py5+5,0,360,5);
          end;
          if pt32=1 then
          begin
          px32:=800;
          point:=point+1;
          pt32:=2;
          end;
          if (px33=x)and(py5=y)then begin pt33:=1; end;
          if pt33=0 then
          begin
          PieSlice(px33,py5+5,0,360,5);
          end;
          if pt33=1 then
          begin
          px33:=800;
          point:=point+1;
          pt33:=2;
          end;
          if (px34=x)and(py6=y)then begin pt34:=1; end;
          if pt34=0 then
          begin
          PieSlice(px34-5,py6+5,0,360,5);
          end;
          if pt34=1 then
          begin
          px34:=800;
          point:=point+1;
          pt34:=2;
          end;
          if (px35=x)and(py6=y)then begin pt35:=1; end;
          if pt35=0 then
          begin
          PieSlice(px35-5,py6+5,0,360,5);
          end;
          if pt35=1 then
          begin
          px35:=800;
          point:=point+1;
          pt35:=2;
          end;
          if (px36=x)and(py6=y)then begin pt36:=1; end;
          if pt36=0 then
          begin
          PieSlice(px36-5,py6+5,0,360,5);
          end;
          if pt36=1 then
          begin
          px36:=800;
          point:=point+1;
          pt36:=2;
          end;
          if (px37=x)and(py6=y)then begin pt37:=1; end;
          if pt37=0 then
          begin
          PieSlice(px37-5,py6+5,0,360,5);
          end;
          if pt37=1 then
          begin
          px37:=800;
          point:=point+1;
          pt37:=2;
          end;
          if (px38=x)and(py6=y)then begin pt38:=1; end;
          if pt38=0 then
          begin
          PieSlice(px38,py6+5,0,360,5);
          end;
          if pt38=1 then
          begin
          px38:=800;
          point:=point+1;
          pt38:=2;
          end;
          if (px39=x)and(py6=y)then begin pt39:=1; end;
          if pt39=0 then
          begin
          PieSlice(px39-5,py6+5,0,360,5);
          end;
          if pt39=1 then
          begin
          px39:=800;
          point:=point+1;
          pt39:=2;
          end;
          if (px40=x)and(py6=y)then begin pt40:=1; end;
          if pt40=0 then
          begin
          PieSlice(px40,py6+5,0,360,5);
          end;
          if pt40=1 then
          begin
          px40:=800;
          point:=point+1;
          pt40:=2;
          end;
          if (px41=x)and(py7=y)then begin pt41:=1; end;
          if pt41=0 then
          begin
          PieSlice(px41,py7+5,0,360,5);
          end;
          if pt41=1 then
          begin
          px41:=800;
          point:=point+1;
          pt41:=2;
          end;
          if (px42=x)and(py7=y)then begin pt42:=1; end;
          if pt42=0 then
          begin
          PieSlice(px42,py7+5,0,360,5);
          end;
          if pt42=1 then
          begin
          px42:=800;
          point:=point+1;
          pt42:=2;
          end;







          if x>655 then
          begin
          x:=-15; y:=85
          end;
          if x<-15 then
          begin
          x:=655; y:=405
          end;
          if a=1 then
          begin
          setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,60,300,13);
          PieSlice(x,y,300,355,7);
          PieSlice(x,y,5,60,7);
          PieSlice(x,y,0,5,12);
          PieSlice(x,y,359,360,12);
          arc2(x,y,0,60,13);
          end;
          if c=1 then
          begin
          setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,0,120,13);
          PieSlice(x,y,240,360,13);
          PieSlice(x,y,185,240,7);
          PieSlice(x,y,120,175,7);
          PieSlice(x,y,175,185,13);
          arc2(x,y,120,180,13);
          end;
          if b=1 then
          begin
          setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,0,30,13);
          PieSlice(x,y,150,360,13);
          PieSlice(x,y,30,85,7);
          PieSlice(x,y,95,150,7);
          PieSlice(x,y,85,95,13);
          arc2(x,y,0,90,13);
          end;
          if d=1 then
          begin
          setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,0,210,13);
          PieSlice(x,y,330,360,13);
          PieSlice(x,y,210,265,7);
          PieSlice(x,y,275,330,7);
          PieSlice(x,y,265,275,13);
          arc2(x,y,270,360,13);
          end;
          END;

PROCEDURE jeu2;
          BEGIN
          if x1>655 then
          begin
          x1:=-15; y1:=85
          end;
          if x1<-15 then
          begin
          x1:=655; y1:=405
          end;
          if a2=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x1,y1,60,300,13);
          PieSlice(x1,y1,300,355,7);
          PieSlice(x1,y1,5,60,7);
          PieSlice(x1,y1,0,5,12);
          PieSlice(x1,y1,359,360,12);
          arc2(x1,y1,0,60,13);
          end;
          if c2=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x1,y1,0,120,13);
          PieSlice(x1,y1,240,360,13);
          PieSlice(x1,y1,185,240,7);
          PieSlice(x1,y1,120,175,7);
          PieSlice(x1,y1,175,185,13);
          arc2(x1,y1,120,180,13);
          end;
          if b2=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x1,y1,0,30,13);
          PieSlice(x1,y1,150,360,13);
          PieSlice(x1,y1,30,85,7);
          PieSlice(x1,y1,95,150,7);
          PieSlice(x1,y1,85,95,13);
          arc2(x1,y1,0,90,13);
          end;
          if d2=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x1,y1,0,210,13);
          PieSlice(x1,y1,330,360,13);
          PieSlice(x1,y1,210,265,7);
          PieSlice(x1,y1,275,330,7);
          PieSlice(x1,y1,265,275,13);
          arc2(x1,y1,270,360,13);
          end;
          END;

PROCEDURE jeu3;
          BEGIN
          if x2>655 then
          begin
          x2:=-15; y2:=85
          end;
          if x2<-15 then
          begin
          x2:=655; y2:=405
          end;
          if a4=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x2,y2,60,300,13);
          PieSlice(x2,y2,300,355,7);
          PieSlice(x2,y2,5,60,7);
          PieSlice(x2,y2,0,5,12);
          PieSlice(x2,y2,359,360,12);
          arc2(x2,y2,0,60,13);
          end;
          if c4=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x2,y2,0,120,13);
          PieSlice(x2,y2,240,360,13);
          PieSlice(x2,y2,185,240,7);
          PieSlice(x2,y2,120,175,7);
          PieSlice(x2,y2,175,185,13);
          arc2(x2,y2,120,180,13);
          end;
          if b4=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x2,y2,0,30,13);
          PieSlice(x2,y2,150,360,13);
          PieSlice(x2,y2,30,85,7);
          PieSlice(x2,y2,95,150,7);
          PieSlice(x2,y2,85,95,13);
          arc2(x2,y2,0,90,13);
          end;
          if d4=1 then
          begin
          setfillstyle(1,color2);
          setcolor(color2);
          PieSlice(x2,y2,0,210,13);
          PieSlice(x2,y2,330,360,13);
          PieSlice(x2,y2,210,265,7);
          PieSlice(x2,y2,275,330,7);
          PieSlice(x2,y2,265,275,13);
          arc2(x2,y2,270,360,13);
          end;
          END;
PROCEDURE guide;
          var ch:char;
          BEGIN
          ch:=readkey;
          if ch=#72 then begin chtt:=72; end;
          if ch=#80 then begin chtt:=80; end;
          if ch=#75 then begin cht:=75; end;
          if ch=#77 then begin cht:=77; end;
          if ch=#32 then
          begin
            cht:=0;
            chtt:=00;
          end;
          if ch=#27 then begin zy:=1; end;
          END;



PROCEDURE commande;

          BEGIN

if(cht=77)and((x<>575)or(y=405))and((x<>65)or(y=65)or(y=245)or(y=415))and((x<>135)or(y=65)or(y=135)or(y=245)or(y=345)or(y=415)
)and((y<205)or(y>275)or(x<>195))and((y<205)or(y>235)or(x<>315))and((y<75)or(y>125)or(x<>315))and((y<355)or(y>405)or(x<>315))
and((y<>245)or(x<>395))and((y<115)or(y>175)or(x<>505))and((y<195)or(y>355)or(x<>505))and((y<255)or(y>275)or(x<>255))
and((y<295)or(y>335)or(x<>375))and((y<255)or(y>305)or(x<>445))and((y<175)or(y>235)or(x<>445)) then
               begin
               a:=1; b:=0; c:=0; d:=0;
               x:=x+10;
               setcolor(0);
               arc2(x,y,90,270,14);
               arc2(x-1,y,90,270,14);
               arc2(x-2,y,90,270,14);
               arc2(x-3,y,90,270,14);
               arc2(x-4,y,90,270,13);
               arc2(x-5,y,90,270,13);
               arc2(x-6,y,90,270,13);
               arc2(x-7,y,90,270,13);
               arc2(x-8,y,90,270,13);
               arc2(x-9,y,90,270,13);
               arc2(x-10,y,90,270,13);
               arc2(x-11,y,90,270,13);
               arc2(x-12,y,90,140,13);
               arc2(x-12,y,220,270,13);
               setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,60,300,13);
          PieSlice(x,y,300,355,7);
          PieSlice(x,y,5,60,7);
          PieSlice(x,y,0,5,12);
          PieSlice(x,y,359,360,12);
          arc2(x,y,0,60,13);
               end;
if(cht=75)and((x<>65)or(y=85))and((y<115)or(y>235)or(x<>135))and((y<255)or(y>365)or(x<>135))and((y<175)or(y>235)or(x<>195))
and((y<255)or(y>305)or(x<>195))and((y<225)or(y>265)or(x<>245))and((y<255)or(y>275)or(x<>255))and((y<295)or(y>335)or(x<>375))
and((y<75)or(y>175)or(x<>575))and((y<195)or(y>405)or(x<>575))and((y<75)or(y>125)or(x<>315))and((y<205)or(y>235)or(x<>315))
and((y<355)or(y>405)or(x<>315))and((y<205)or(y>275)or(x<>445))and((y<145)or(y>235)or(x<>505))and((y<255)or(y>335)or(x<>505))
then
               begin
               x:=x-10;
               a:=0; b:=0; c:=1; d:=0;
               setcolor(0);
               arc2(x,y,270,90,14);
               arc2(x+1,y,270,90,14);
               arc2(x+2,y,270,90,14);
               arc2(x+3,y,270,90,14);
               arc2(x+4,y,270,90,13);
               arc2(x+5,y,270,90,13);
               arc2(x+6,y,270,90,13);
               arc2(x+7,y,270,90,13);
               arc2(x+8,y,270,90,13);
               arc2(x+9,y,270,90,13);
               arc2(x+10,y,270,90,13);
               arc2(x+11,y,270,90,13);
               arc2(x+12,y,270,320,13);
               arc2(x+12,y,40,90,13);
               setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,0,120,13);
          PieSlice(x,y,240,360,13);
          PieSlice(x,y,185,240,7);
          PieSlice(x,y,120,175,7);
          PieSlice(x,y,175,185,13);
          arc2(x,y,120,180,13);
               end;
if(chtt=72)and(y<>65)and((y<>415)or(x=315)or(x=65)or(x=575))and((y<>85)or(x>60))and((x<75)or(x>305)or(y<>135))
and((x<325)or(x>525)or(y<>135))and((x<515)or(x>565)or(y<>185))and((x<175)or(x>465)or(y<>195))
and((x<225)or(x>305)or(y<>245))and((x<325)or(x>415)or(y<>245))and((x<75)or(x>125)or(y<>245))
and((x<145)or(x>185)or(y<>245))and((x<455)or(x>495)or(y<>245))and((x<265)or(x>435)or(y<>285))
and((x<205)or(x>245)or(y<>285))and((x<385)or(x>495)or(y<>345))and((x<145)or(x>365)or(y<>345))
and((x<585)or(y<>405))
then
               begin
               a:=0; b:=1; c:=0; d:=0;
               y:=y-10;
               setcolor(0);
               arc2(x,y,180,360,14);
               arc2(x,y+1,180,360,14);
               arc2(x,y+2,180,360,14);
               arc2(x,y+3,180,360,14);
               arc2(x,y+4,180,360,13);
               arc2(x,y+5,180,360,13);
               arc2(x,y+6,180,360,13);
               arc2(x,y+7,180,360,13);
               arc2(x,y+8,180,360,13);
               arc2(x,y+9,180,360,13);
               arc2(x,y+10,180,360,13);
               arc2(x,y+11,180,360,13);
               arc2(x,y+12,180,230,13);
               arc2(x,y+12,290,360,13);
               setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,0,30,13);
          PieSlice(x,y,150,360,13);
          PieSlice(x,y,30,85,7);
          PieSlice(x,y,95,150,7);
          PieSlice(x,y,85,95,13);
          arc2(x,y,0,90,13);
               end;
if(chtt=80)and(y<>415)and((x<75)or(x>305)or(y<>65))and((x<325)or(x>565)or(y<>65))and((x<145)or(x>495)or(y<>135))
and((x<515)or(x>565)or(y<>185))and((x<205)or(x>305)or(y<>195))and((x<325)or(x>435)or(y<>195))and((x<225)or(x>245)or(y<>245))
and((x<265)or(x>415)or(y<>245))and((x<145)or(x>185)or(y<>245))and((x<455)or(x>495)or(y<>245))and((x<75)or(x>125)or(y<>245))
and((x<175)or(x>365)or(y<>285))and((x<385)or(x>465)or(y<>285))and((x<105)or(x>305)or(y<>345))and((x<325)or(x>525)or(y<>345))
and((x<585)or(y<>405))and((x>55)or(y<>85))then
               begin
               a:=0; b:=0; c:=0; d:=1;
               y:=y+10;
               setcolor(0);
               arc2(x,y,0,180,14);
               arc2(x,y-1,0,180,14);
               arc2(x,y-2,0,180,14);
               arc2(x,y-3,0,180,14);
               arc2(x,y-4,0,180,13);
               arc2(x,y-5,0,180,13);
               arc2(x,y-6,0,180,13);
               arc2(x,y-7,0,180,13);
               arc2(x,y-8,0,180,13);
               arc2(x,y-9,0,180,13);
               arc2(x,y-10,0,180,13);
               arc2(x,y-11,0,180,13);
               arc2(x,y-12,0,70,13);
               arc2(x,y-12,110,180,13);
               setfillstyle(1,color);
          setcolor(color);
          PieSlice(x,y,0,210,13);
          PieSlice(x,y,330,360,13);
          PieSlice(x,y,210,265,7);
          PieSlice(x,y,275,330,7);
          PieSlice(x,y,265,275,13);
          arc2(x,y,270,360,13);
               end;

          END;
Procedure phantom;
          begin
          if pouv=0 then
          begin
          if ((y1<y)and(si4=0))or(si4=1) then begin d1:=1; b1:=0; end;
          if ((y1>y)and(si2=0))or(si2=1) then begin d1:=0; b1:=1; end;
          if ((x1<x)and(si1=0))or(si1=1) then begin c1:=0; a1:=1; end;
          if ((x1>x)and(si3=0))or(si3=1) then begin c1:=1; a1:=0; end;
          end;
          if pouv=1 then
          begin
          if ((y1>=y)and(y<=240)) then begin d1:=1; b1:=0; si4:=1; end;
          if ((y1<=y)and(y>240)) then begin d1:=0; b1:=1; si2:=1; end;
          if ((x1>=x)and(x<=320)) then begin c1:=0; a1:=1; si1:=1; end;
          if ((x1<=x)and(x>320)) then begin c1:=1; a1:=0; si3:=1 end;
          end;

if(a1=1)and((x1<>x)or(si1=1))and((x1<>575)or(y1=405))and((x1<>65)or(y1=65)or(y1=245)or(y1=415))and((x1<>135)or(y1=65)or(y1=135)
or(y1=245)
or(y1=345)or(y1=415))and((y1<205)or(y1>275)or(x1<>195))and((y1<205)or(y1>235)or(x1<>315))and((y1<75)or(y1>125)or(x1<>315))and
((y1<355)or(y1>405)or(x1<>315))
and((y1<>245)or(x1<>395))and((y1<115)or(y1>175)or(x1<>505))and((y1<195)or(y1>355)or(x1<>505))and((y1<255)or(y1>275)or(x1<>255))
and((y1<295)or(y1>335)or(x1<>375))and((y1<255)or(y1>305)or(x1<>445))and((y1<175)or(y1>235)or(x1<>445))and(x1<>575) then
               begin
               a2:=1; b2:=0; c2:=0; d2:=0;
               x1:=x1+10;
               setcolor(0);
               arc2(x1,y1,90,270,14);
               arc2(x1-1,y1,90,270,14);
               arc2(x1-2,y1,90,270,14);
               arc2(x1-3,y1,90,270,14);
               arc2(x1-4,y1,90,270,13);
               arc2(x1-5,y1,90,270,13);
               arc2(x1-6,y1,90,270,13);
               arc2(x1-7,y1,90,270,13);
               arc2(x1-8,y1,90,270,13);
               arc2(x1-9,y1,90,270,13);
               arc2(x1-10,y1,90,270,13);
               arc2(x1-11,y1,90,270,13);
               arc2(x1-12,y1,90,140,13);
               arc2(x1-12,y1,220,270,13);
               end;
if(c1=1)and((x1<>x)or(si3=1))and((x1<>65)or(y1=85))and((y1<115)or(y1>235)or(x1<>135))and((y1<255)or(y1>365)or(x1<>135))and
((y1<175)or
(y1>235)
or(x1<>195))and((y1<45)or(y1>305)or(x1<>65))
and((y1<255)or(y1>305)or(x1<>195))and((y1<225)or(y1>265)or(x1<>245))and((y1<255)or(y1>275)or(x1<>255))and((y1<295)or(y1>335)
or(x1<>375))
and((y1<75)or(y1>175)or(x1<>575))and((y1<195)or(y1>405)or(x1<>575))and((y1<75)or(y1>125)or(x1<>315))and((y1<205)or(y1>235)
or(x1<>315))
and((y1<355)or(y1>405)or(x1<>315))and((y1<205)or(y1>275)or(x1<>445))and((y1<145)or(y1>235)or(x1<>505))and((y1<255)or(y1>335)
or(x1<>505))
then
               begin
               a2:=0; b2:=0; c2:=1; d2:=0;
               x1:=x1-10;
               setcolor(0);
               arc2(x1,y1,270,90,14);
               arc2(x1+1,y1,270,90,14);
               arc2(x1+2,y1,270,90,14);
               arc2(x1+3,y1,270,90,14);
               arc2(x1+4,y1,270,90,13);
               arc2(x1+5,y1,270,90,13);
               arc2(x1+6,y1,270,90,13);
               arc2(x1+7,y1,270,90,13);
               arc2(x1+8,y1,270,90,13);
               arc2(x1+9,y1,270,90,13);
               arc2(x1+10,y1,270,90,13);
               arc2(x1+11,y1,270,90,13);
               arc2(x1+12,y1,270,320,13);
               arc2(x1+12,y1,40,90,13);
               end;
if(b1=1)and((y1<>y)or(si2=1))and(y1<>65)and((y1<>415)or(x1=315)or(x1=65)or(x1=575))and((y1<>85)or(x1>60))and((x1<75)or(x1>305)
or(y1<>135))
and((x1<325)or(x1>525)or(y1<>135))and((x1<515)or(x1>565)or(y1<>185))and((x1<175)or(x1>465)or(y1<>195))
and((x1<225)or(x1>305)or(y1<>245))and((x1<325)or(x1>415)or(y1<>245))and((x1<75)or(x1>125)or(y1<>245))
and((x1<145)or(x1>185)or(y1<>245))and((x1<455)or(x1>495)or(y1<>245))and((x1<265)or(x1>435)or(y1<>285))
and((x1<205)or(x1>245)or(y1<>285))and((x1<385)or(x1>495)or(y1<>345))and((x1<145)or(x1>365)or(y1<>345))
and((x1<585)or(y1<>405))
then
               begin
               a2:=0; b2:=1; c2:=0; d2:=0;
               y1:=y1-10;
               setcolor(0);
               arc2(x1,y1,180,360,14);
               arc2(x1,y1+1,180,360,14);
               arc2(x1,y1+2,180,360,14);
               arc2(x1,y1+3,180,360,14);
               arc2(x1,y1+4,180,360,13);
               arc2(x1,y1+5,180,360,13);
               arc2(x1,y1+6,180,360,13);
               arc2(x1,y1+7,180,360,13);
               arc2(x1,y1+8,180,360,13);
               arc2(x1,y1+9,180,360,13);
               arc2(x1,y1+10,180,360,13);
               arc2(x1,y1+11,180,360,13);
               arc2(x1,y1+12,180,230,13);
               arc2(x1,y1+12,290,360,13);
               end;
if(d1=1)and((y1<>y)or(si4=1))and(y1<>415)and((x1<75)or(x1>305)or(y1<>65))and((x1<325)or(x1>565)or(y1<>65))and((x1<145)
or(x1>495)or
(y1<>135))
and((x1<515)or(x1>565)or(y1<>185))and((x1<205)or(x1>305)or(y1<>195))and((x1<325)or(x1>435)or(y1<>195))and((x1<225)or(x1>245)
or(y1<>245))
and((x1<265)or(x1>415)or(y1<>245))and((x1<145)or(x1>185)or(y1<>245))and((x1<455)or(x1>495)or(y1<>245))and((x1<75)or(x1>125)
or(y1<>245))
and((x1<175)or(x1>365)or(y1<>285))and((x1<385)or(x1>465)or(y1<>285))and((x1<105)or(x1>305)or(y1<>345))and((x1<325)or(x1>525)
or(y1<>345))
and((x1<585)or(y1<>405))and((x1>55)or(y1<>85))then
               begin
               a2:=0; b2:=0; c2:=0; d2:=1;
               y1:=y1+10;
               setcolor(0);
               arc2(x1,y1,0,180,14);
               arc2(x1,y1-1,0,180,14);
               arc2(x1,y1-2,0,180,14);
               arc2(x1,y1-3,0,180,14);
               arc2(x1,y1-4,0,180,13);
               arc2(x1,y1-5,0,180,13);
               arc2(x1,y1-6,0,180,13);
               arc2(x1,y1-7,0,180,13);
               arc2(x1,y1-8,0,180,13);
               arc2(x1,y1-9,0,180,13);
               arc2(x1,y1-10,0,180,13);
               arc2(x1,y1-11,0,180,13);
               arc2(x1,y1-12,0,70,13);
               arc2(x1,y1-12,110,180,13);
               end;
          end;
Procedure phantom2;
          begin
          if pouv=0 then
          begin
          if ((y2<y)and(sit4=0))or(sit4=1) then begin d3:=1; b3:=0; end;
          if ((y2>y)and(sit2=0))or(sit2=1) then begin d3:=0; b3:=1; end;
          if ((x2<x)and(sit1=0))or(sit1=1) then begin c3:=0; a3:=1; end;
          if ((x2>x)and(sit3=0))or(sit3=1) then begin c3:=1; a3:=0; end;
          end;
          if pouv=1 then
          begin
          if ((y2>=y)and(y<=240)) then begin d3:=1; b3:=0; sit4:=1; end;
          if ((y2<=y)and(y>240)) then begin d3:=0; b3:=1; sit2:=1; end;
          if ((x2>=x)and(x<=320)) then begin c3:=0; a3:=1; sit1:=1; end;
          if ((x2<=x)and(x>320)) then begin c3:=1; a3:=0; sit3:=1 end;
          end;

if(a3=1)and((x2<>x)or(sit1=1))and((x2<>575)or(y2=405))and((x2<>65)or(y2=65)or(y2=245)or(y2=415))and((x2<>135)or(y2=65)
or(y2=135)
or(y2=245)
or(y2=345)or(y2=415))and((y2<205)or(y2>275)or(x2<>195))and((y2<205)or(y2>235)or(x2<>315))and((y2<75)or(y2>125)or(x2<>315))and
((y2<355)or(y2>405)or(x2<>315))
and((y2<>245)or(x2<>395))and((y2<115)or(y2>175)or(x2<>505))and((y2<195)or(y2>355)or(x2<>505))and((y2<255)or(y2>275)or(x2<>255))
and((y2<295)or(y2>335)or(x2<>375))and((y2<255)or(y2>305)or(x2<>445))and((y2<175)or(y2>235)or(x2<>445))and(x2<>575) then
               begin
               a4:=1; b4:=0; c4:=0; d4:=0;
               x2:=x2+10;
               setcolor(0);
               arc2(x2,y2,90,270,14);
               arc2(x2-1,y2,90,270,14);
               arc2(x2-2,y2,90,270,14);
               arc2(x2-3,y2,90,270,14);
               arc2(x2-4,y2,90,270,13);
               arc2(x2-5,y2,90,270,13);
               arc2(x2-6,y2,90,270,13);
               arc2(x2-7,y2,90,270,13);
               arc2(x2-8,y2,90,270,13);
               arc2(x2-9,y2,90,270,13);
               arc2(x2-10,y2,90,270,13);
               arc2(x2-11,y2,90,270,13);
               arc2(x2-12,y2,90,140,13);
               arc2(x2-12,y2,220,270,13);
               end;
if(c3=1)and((x2<>x)or(sit3=1))and((x2<>65)or(y2=85))and((y2<115)or(y2>235)or(x2<>135))and((y2<255)or(y2>365)or(x2<>135))
and((y2<175)or
(y2>235)
or(x2<>195))
and((y2<255)or(y2>305)or(x2<>195))and((y2<225)or(y2>265)or(x2<>245))and((y2<255)or(y2>275)or(x2<>255))and((y2<295)or(y2>335)
or(x2<>375))
and((y2<75)or(y2>175)or(x2<>575))and((y2<195)or(y2>405)or(x2<>575))and((y2<75)or(y2>125)or(x2<>315))and((y2<205)or(y2>235)
or(x2<>315))
and((y2<355)or(y2>405)or(x2<>315))and((y2<205)or(y2>275)or(x2<>445))and((y2<145)or(y2>235)or(x2<>505))and((y2<255)or(y2>335)
or(x2<>505))and(x2<>65)
then
               begin
               a4:=0; b4:=0; c4:=1; d4:=0;
               x2:=x2-10;
               setcolor(0);
               arc2(x2,y2,270,90,14);
               arc2(x2+1,y2,270,90,14);
               arc2(x2+2,y2,270,90,14);
               arc2(x2+3,y2,270,90,14);
               arc2(x2+4,y2,270,90,13);
               arc2(x2+5,y2,270,90,13);
               arc2(x2+6,y2,270,90,13);
               arc2(x2+7,y2,270,90,13);
               arc2(x2+8,y2,270,90,13);
               arc2(x2+9,y2,270,90,13);
               arc2(x2+10,y2,270,90,13);
               arc2(x2+11,y2,270,90,13);
               arc2(x2+12,y2,270,320,13);
               arc2(x2+12,y2,40,90,13);
               end;
if(b3=1)and((y2<>y)or(sit2=1))and(y2<>65)and((y2<>415)or(x2=315)or(x2=65)or(x2=575))and((y2<>85)or(x2>60))and((x2<75)or(x2>305)
or(y2<>135))
and((x2<325)or(x2>525)or(y2<>135))and((x2<515)or(x2>565)or(y2<>185))and((x2<175)or(x2>465)or(y2<>195))
and((x2<225)or(x2>305)or(y2<>245))and((x2<325)or(x2>415)or(y2<>245))and((x2<75)or(x2>125)or(y2<>245))
and((x2<145)or(x2>185)or(y2<>245))and((x2<455)or(x2>495)or(y2<>245))and((x2<265)or(x2>435)or(y2<>285))
and((x2<205)or(x2>245)or(y2<>285))and((x2<385)or(x2>495)or(y2<>345))and((x2<145)or(x2>365)or(y2<>345))
and((x2<585)or(y2<>405))
then
               begin
               a4:=0; b4:=1; c4:=0; d4:=0;
               y2:=y2-10;
               setcolor(0);
               arc2(x2,y2,180,360,14);
               arc2(x2,y2+1,180,360,14);
               arc2(x2,y2+2,180,360,14);
               arc2(x2,y2+3,180,360,14);
               arc2(x2,y2+4,180,360,13);
               arc2(x2,y2+5,180,360,13);
               arc2(x2,y2+6,180,360,13);
               arc2(x2,y2+7,180,360,13);
               arc2(x2,y2+8,180,360,13);
               arc2(x2,y2+9,180,360,13);
               arc2(x2,y2+10,180,360,13);
               arc2(x2,y2+11,180,360,13);
               arc2(x2,y2+12,180,230,13);
               arc2(x2,y2+12,290,360,13);
               end;
if(d3=1)and((y2<>y)or(sit4=1))and(y2<>415)and((x2<75)or(x2>305)or(y2<>65))and((x2<325)or(x2>565)or(y2<>65))and((x2<145)or
(x2>495)or
(y2<>135))
and((x2<515)or(x2>565)or(y2<>185))and((x2<205)or(x2>305)or(y2<>195))and((x2<325)or(x2>435)or(y2<>195))and((x2<225)or(x2>245)
or(y2<>245))
and((x2<265)or(x2>415)or(y2<>245))and((x2<145)or(x2>185)or(y2<>245))and((x2<455)or(x2>495)or(y2<>245))and((x2<75)or(x2>125)
or(y2<>245))
and((x2<175)or(x2>365)or(y2<>285))and((x2<385)or(x2>465)or(y2<>285))and((x2<105)or(x2>305)or(y2<>345))and((x2<325)or(x2>525)
or(y2<>345))
and((x2<585)or(y2<>405))and((x2>55)or(y2<>85))then
               begin
               a4:=0; b4:=0; c4:=0; d4:=1;
               y2:=y2+10;
               setcolor(0);
               arc2(x2,y2,0,180,14);
               arc2(x2,y2-1,0,180,14);
               arc2(x2,y2-2,0,180,14);
               arc2(x2,y2-3,0,180,14);
               arc2(x2,y2-4,0,180,13);
               arc2(x2,y2-5,0,180,13);
               arc2(x2,y2-6,0,180,13);
               arc2(x2,y2-7,0,180,13);
               arc2(x2,y2-8,0,180,13);
               arc2(x2,y2-9,0,180,13);
               arc2(x2,y2-10,0,180,13);
               arc2(x2,y2-11,0,180,13);
               arc2(x2,y2-12,0,70,13);
               arc2(x2,y2-12,110,180,13);
               end;
          end;
BEGIN
     opengraph;
     logo;
     titre;
     clearviewport;
     setlinestyle(0,0,NormWidth);
     parametre;

     5:if zzzz=0 then
          begin
          aaa:=180; bbb:=207;
          azaz:=1;
          end;
     if zzzz=1 then
          begin
          aaa:=180; bbb:=187;
          azaz:=0;
          end;
          if gagner=1 then
          begin
          aaa:=180; bbb:=257;
          azaz:=1;
          gagner:=0;
          end;
          clearviewport;

     menu;
     2:entre;
     if keypressed then begin commande2; end;
     if ((azer=1)and(azaz=0)) then begin azer:=0; goto 6; end;
     if ((azer=1)and(azaz=1)) then begin azer:=0; goto 3; end;
     if ((azer=1)and(azaz=2)) then begin azer:=0; goto 4; end;
     goto 2;
     3:parametre;
     6:decor;
     1:if zy=1 then
     begin
     zy:=0;
     zzzz:=1;
     goto 5;
     end;
     setcolor(0);
     setfillstyle(1,0);
     bar(90,5,139,15);
     setcolor(14);
     outtextxy(100,8,(chaine(tppouv)));
     setcolor(0);
     setfillstyle(1,0);
     bar(225,5,259,15);
     setcolor(14);
     outtextxy(225,8,(chaine(score)));
     setcolor(0);
     setfillstyle(1,0);
     bar(310,5,379,15);
     setcolor(14);
     outtextxy(320,8,(chaine(vie)));
     if (pouv=1)and(tppouv=0)and(time=0)then
     begin
     tppouv:=500;
     end;
     if tppouv<>0 then
     begin
     tppouv:=tppouv-1;
     time:=0;
     end;
     setfillstyle(11,13);
          bar(302,122,328,148);
     setfillstyle(11,14);
          bar(302,402,328,428);
     if tppouv=0 then
     begin pouv:=0;
      end;
     setfillstyle(5,col1);
          bar(52,52,78,78);
          setfillstyle(5,col2);
          bar(562,52,588,78);
          setfillstyle(5,col3);
          bar(52,402,78,428);
          setfillstyle(5,col4);
          bar(562,402,588,428);
     if(x=65)and(y=65)and(po1=0)and(pouv<>1)then begin po1:=1; pouv:=1; col1:=2;end;
     if(x=575)and(y=65)and(po2=0)and(pouv<>1)then begin po2:=1;pouv:=1;col2:=2; end;
     if(x=65)and(y=415)and(po3=0)and(pouv<>1)then begin po3:=1; pouv:=1; col3:=2;end;
     if(x=575)and(y=415)and(po4=0)and(pouv<>1)then begin po4:=1; pouv:=1; col4:=2; end;
     if pouv=1 then begin color2:=13 end;
     if pouv=0 then begin color2:=8 end;
     if (((x=x1)and(y=y1))or((x=x2)and(y=y2)))and(pouv=0)then
     begin
      vie:=vie-1;
      x:=315;
      y:=415;
      tppouv:=350;
      time:=1;
      pouv:=1;
     end;
     if (point=42)or(vie<0) then
     begin
      gagne;
      end;


     if keypressed then begin guide; end;

     dea:=a; deb:=b; dec:=c; ded:=d;
     dex:=x;
     dey:=y;
     commande;
     if ((x<>dex)and((deb=1)or(ded=1))) then
     begin
     chtt:=0;
     end;


     if ((y<>dey)and((dea=1)or(dec=1))) then
     begin
     cht:=0;
     end;


     jeu;
     if (x=x1)and(y=y1)and(pouv=1) then
     begin
         score:=score+1; x1:=315; y1:=135;
         calc:=calc+1;
     end;
     if (x=x2)and(y=y2)and(pouv=1) then
     begin
         score:=score+1; x2:=315; y2:=135;
         calc:=calc+1;
     end;
     jeu2;
     jeu3;

     tp:=tp+1;

     if tp=dur then
     begin
          tx1:=x1; ty1:=y1;
          phantom;
          jeu2;
          if tp1=0 then
          begin
               tx2:=x2; ty2:=y2;
               phantom2;
               jeu3;
          end;
          if ((x1>x2-26)and(x1<x2+26))and((y1>y2-26)and(y1<y2+26))then
          begin
               tp1:=30;
          end;
          if tp1<>0 then
          begin
               tp1:=tp1-1;
          end;
          tp:=0;
     end;
     if pouv=0 then
        begin
        if ((tx1=x1) and (ty1=y1)) then
        begin
        chron:=chron+1;
        end;
        if chron=10 then
        begin
          chron:=0;
          es:=0;
          test:=x1;
          test1:=y1;

          if (y1<>y)and(x1<=320) then begin si1:=1; si3:=3; end;
          if (y1<>y)and(x1>320) then begin si1:=3; si3:=1; end;
          if (x1<>x)and(y1>=245) then begin si4:=1; si2:=3; end;
          if (x1<>x)and(y1<245) then begin si4:=3; si2:=1; end;
          if (y1=y)and(y1<=245) then begin si2:=1; si4:=3; end;
          if (y1=y)and(y1>245) then begin si2:=3; si4:=1; end;
          if (x1=x)and(x1<=320) then begin si3:=1; si1:=3; end;
          if (x1=x)and(x1>320) then begin si3:=3; si1:=1; end;
           sauf:=0;
           et:=0;
        end;
        if (test<>x1)and(sauf=0) then begin si2:=0; si4:=0; test:=0; end;
        if (test1<>y1)and(sauf=0) then begin si3:=0; si1:=0; test1:=0; end;

     if ((tx2=x2) and (ty2=y2)) then
     begin
          est:=0;
          testt:=x2;
          test1t:=y2;
          if (y2<>y)and(x2<=320) then begin sit1:=1; sit3:=3; end;
          if (y2<>y)and(x2>320) then begin sit1:=3; sit3:=1; end;
          if (x2<>x)and(y2<=240) then begin sit4:=1; sit2:=3; end;
          if (x2<>x)and(y2>240) then begin sit4:=3; sit2:=1; end;
     end;
     if (testt<>x2) then begin sit2:=0; sit4:=0; testt:=0 end;
     if (test1t<>y2) then begin sit3:=0; sit1:=0; test1t:=0 end;
     end;
          if (x1=245)and (y1=245) and (y>=y1) then
          begin
          si1:=1; si2:=3; si3:=3; si4:=1; sauf:=1;
          end;
          if (x1=245)and (y1=245) and (y<y1) then
          begin
          si1:=1; si2:=1; si3:=3; si4:=3; sauf:=1;
          end;
          if (x1=395)and (y1=245) and (y>=y1) then
          begin
          si1:=3; si2:=3; si3:=1; si4:=1; sauf:=1;
          end;
          if (x1=395)and (y1=245) and (y<y1) then
          begin
          si1:=3; si2:=1; si3:=1; si4:=3; sauf:=1;
          end;
          if (x1=195)and (y1=195) then
          begin
          si1:=3; si2:=3; si3:=1; si4:=1; sauf:=1; et:=1;
          end;
          if (x1=135)and (y1=255)and (et=1) then
          begin
          si1:=3; si2:=1; si3:=3; si4:=3; sauf:=1;
          end;
          if (x1=65) and (y1=65) and (y=65) then
          begin
          sauf:=0; et:=0;
          end;
     goto 1;
 4:clearviewport;
 setcolor(15);
 settextstyle(0,0,1);
 outtextxy(220,100,'Programmer en: TURBO PASCAL.7');
 outtextxy(220,115,'Produit par  : TAKAMI');
 outtextxy(220,130,'Fondateur    : QUESADA Eric');
 outtextxy(220,145,'Dirigeant    : QUESADA Eric');
 outtextxy(220,160,'Graphisme    : QUESADA Eric');
 outtextxy(220,175,'Musique      : Y en a pas!!!');
 outtextxy(220,190,'Intro        : QUESADA Eric');
 outtextxy(220,205,'1e Assistant : QUESADA Eric');
 outtextxy(220,220,'2e Assistant : QUESADA Eric');
 outtextxy(220,235,'3e Assistant : QUESADA Eric');
 outtextxy(220,250,'4e Assistant : QUESADA Eric');
 outtextxy(220,275,'                              @1999');
 settextstyle(0,0,2);
 outtextxy(25,300,'P.S: Comme on dit dans les entreprises');
 outtextxy(50,350,'     Takami:"l''essayer c''est ...');
 readkey;
 delay(500);
 clearviewport;
 settextstyle(0,0,3);
 outtextxy(25,200,' LE METTRE A LA POUBELLE');
 readkey;
 delay(500);
 END.

