{$mode fpc}
PROGRAM superpong;
uses graph,eric,crt;
var xbal,ybal,vecxbal,vecybal,jou1x,jou1y,testjou1x,testjou1y,vecxjou1,
    vecyjou1,turbo,tturbo,efturbo,jou2x,jou2y,testjou2x,testjou2y,vecxjou2,
    vecyjou2,ttturbo,scorj1,scorj2,a,b,term,th,posxia,posyia,auto:integer;
    nbre:real;
label 1;

PROCEDURE logo;
          BEGIN
          setcolor(1);
          line(470,128,350,128);line(350,128,505,163);line(470,128,505,163);
          line(475,126,510,20); line(510,20,510,161); line(475,126,510,161);
          line(515,115,515,170); line(515,115,580,250); line(515,170,580,250);
          arc2(373,392,90,270,16); arc2(530,392,270,90,16);
          line(373,376,530,376);  line(373,408,530,408);
          TEXTCOLOR (1);
          setcolor(1);
          settextstyle(10,0,7);
          outtextxy(100,150,'TAKAMI');
          settextstyle(1,0,4);
          outtextxy(370,370,'Production');
          setfillstyle(2,1);
          floodfill(0,0,1);
          floodfill(195,235,1);
          floodfill(335,235,1);
          delay(2500);
          closegraph;
          opengraph;
          setcolor(15);
          settextstyle(10,0,3);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(7);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(8);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          setcolor(0);
          outtextxy(220,150,'PRESENTE');
          delay(400);
          settextstyle(0,0,0);
         END;

PROCEDURE calcul(posx,posy,vecx,vecy,jouadx,jouady,posxb:integer);
BEGIN
term:=0;
while term=0 do
begin
posx:=posx+vecx;
posy:=posy+vecy;

if (posy>jouady+3)and(posy<=jouady+20)and(posx<jouadx-3)and(posx>jouadx-8)and(vecx>0)then
     begin vecy:=-1; vecx:=-1; end;
if (posy>jouady+20)and(posy<=jouady+37)and(posx<jouadx-3)and(posx>jouadx-8)and(vecx>0)then
     begin vecy:=1; vecx:=-1;end;
if (posy>jouady-8)and(posy<=jouady-3)and(posx<jouadx-3)and(posx>jouadx-8)and(vecx>0) then
    begin vecy:=-3; vecx:=-1;end;
if (posy>jouady+43)and(posy<=jouady+48)and(posx<jouadx-3)and(posx>jouadx-8)and(vecx>0) then
    begin vecy:=3; vecx:=-1; end;
if (posy>jouady-4)and(posy<=jouady+3)and(posx<jouadx-3)and(posx>jouadx-8)and(vecx>0) then begin vecy:=-2; vecx:=-1; end;
if (posy>jouady+37)and(posy<=jouady+43)and(posx<jouadx-3)and(posx>jouadx-8)and(vecx>0) then begin vecy:=2; vecx:=-1; end;


if posx>=625 then begin vecx:=-1; turbo:=0;
     if vecy>0 then
     begin nbre:=vecy; vecy:=trunc(nbre/nbre); end
     else
     begin nbre:=vecy; vecy:=-trunc(nbre/nbre); end;
     end;



if posx<=15 then
     begin
     term:=2;
     end;

if posy>=465 then begin nbre:=vecy*-1; vecy:=trunc(nbre); end;
if posy<=15 then begin nbre:=vecy*-1; vecy:=trunc(nbre); end;
end;
term:=th;

posxia:=posx;
posyia:=posy;

END;

PROCEDURE ordi;
BEGIN
Calcul(xbal,ybal,vecxbal,vecybal,jou2x,jou2y,jou1x);

if posyia>(jou1y+20) then
     begin
     vecyjou1:=1;

     tturbo:=0;
     end;

if posyia<(jou1y+20) then
     begin
     vecyjou1:=-1;

     tturbo:=0;
     end;

if posxia>(jou1x) then
     begin

     vecxjou1:=1;
     tturbo:=0;
     end;

if posxia<(jou1x) then
     begin

     vecxjou1:=-1;
     tturbo:=0;
     end;

END;

PROCEDURE parametre;
BEGIN
auto:=0;
scorj1:=0;
scorj2:=0;
efturbo:=0;
ttturbo:=0;
tturbo:=0;
turbo:=0;
xbal:=20;
ybal:=20;
vecxbal:=1;
vecybal:=1;
jou1x:=15;
jou1y:=220;
vecxjou1:=0;
vecyjou1:=0;
jou2x:=625;
jou2y:=220;
vecxjou2:=0;
vecyjou2:=0;
END;

PROCEDURE terrain;
BEGIN
  if (efturbo=1)and(turbo=0)then begin efturbo:=0; clearviewport; end;
  setfillstyle(0,0);
  bar(0,0,30,9);
  bar(610,0,640,9);
  setcolor(12);
  outtextxy(0,0,chaine(scorj1));
  outtextxy(620,0,chaine(scorj2));
  circle(xbal,ybal,5);
  setcolor(10);
  line(jou1x,jou1y,jou1x,jou1y+40);
  line(jou2x,jou2y,jou2x,jou2y+40);
  setcolor(15);
  rectangle(10,10,630,470);
  line(320,10,320,470);
  circle(320,240,50);
  testjou1x:=jou1x;
  testjou1y:=jou1y;
  testjou2x:=jou2x;
  testjou2y:=jou2y;
 END;

PROCEDURE touche;
var ch:char;
BEGIN
ch:=readkey;

if ch=#253 then
   begin
   if auto=1 then
   begin auto:=0 end
   else
   begin auto:=1; end;
   end;

if ch=#27 then
     begin
     closegraph;
     circle(0,0,0);
     end;

if ch=#97 then
     begin
     vecyjou1:=-1;
     vecxjou1:=0;
     tturbo:=0;
     end;

if ch=#113 then
     begin
     vecyjou1:=1;
     vecxjou1:=0;
     tturbo:=0;
     end;

if ch=#119 then
     begin
     vecxjou1:=-1;
     vecyjou1:=0;
     tturbo:=1;
     end;

if ch=#120 then
     begin
     vecxjou1:=1;
     vecyjou1:=0;
     if tturbo=1 then begin tturbo:=2; end;
     end;

if ch=#72 then
     begin
     vecyjou2:=-1;
     vecxjou2:=0;
     ttturbo:=0;
     end;

if ch=#80 then
     begin
     vecyjou2:=1;
     vecxjou2:=0;
     ttturbo:=0;
     end;

if ch=#77 then
     begin
     vecxjou2:=1;
     vecyjou2:=0;
     ttturbo:=1;
     end;

if ch=#75 then
     begin
     vecxjou2:=-1;
     vecyjou2:=0;
     if ttturbo=1 then begin ttturbo:=2; end;
     end;

END;

PROCEDURE Bord;
BEGIN
if (ybal>jou1y+3)and(ybal<=jou1y+20)and(xbal<jou1x+8)and(xbal>jou1x+3)and(vecxbal<0)then
     begin vecybal:=-1; vecxbal:=1;end;
if (ybal>jou1y+20)and(ybal<=jou1y+37)and(xbal<jou1x+8)and(xbal>jou1x+3)and(vecxbal<0)then
     begin vecybal:=1; vecxbal:=1;end;
if (ybal>jou1y-8)and(ybal<=jou1y-3)and(xbal<jou1x+8)and(xbal>jou1x+3)and(vecxbal<0) then
    begin vecybal:=-3; vecxbal:=1;end;
if (ybal>jou1y+43)and(ybal<=jou1y+48)and(xbal<jou1x+8)and(xbal>jou1x+3)and(vecxbal<0) then
    begin vecybal:=3; vecxbal:=1; end;
if (ybal>jou1y-4)and(ybal<=jou1y+3)and(xbal<jou1x+8)and(xbal>jou1x+3)and(vecxbal<0) then begin vecybal:=-2; vecxbal:=1; end;
if (ybal>jou1y+37)and(ybal<=jou1y+43)and(xbal<jou1x+8)and(xbal>jou1x+3)and(vecxbal<0) then begin vecybal:=2; vecxbal:=1; end;
if (ybal>jou1y-8)and(ybal<=jou1y+48)and(xbal<jou1x+8)and(xbal>jou1x+3) then
begin
turbo:=0;
if tturbo=2 then begin turbo:=1; efturbo:=1; vecxbal:=2; end;
end;

if (ybal>jou2y+3)and(ybal<=jou2y+20)and(xbal<jou2x-3)and(xbal>jou2x-8)and(vecxbal>0)then
     begin vecybal:=-1; vecxbal:=-1; end;
if (ybal>jou2y+20)and(ybal<=jou2y+37)and(xbal<jou2x-3)and(xbal>jou2x-8)and(vecxbal>0)then
     begin vecybal:=1; vecxbal:=-1;end;
if (ybal>jou2y-8)and(ybal<=jou2y-3)and(xbal<jou2x-3)and(xbal>jou2x-8)and(vecxbal>0) then
    begin vecybal:=-3; vecxbal:=-1;end;
if (ybal>jou2y+43)and(ybal<=jou2y+48)and(xbal<jou2x-3)and(xbal>jou2x-8)and(vecxbal>0) then
    begin vecybal:=3; vecxbal:=-1; end;
if (ybal>jou2y-4)and(ybal<=jou2y+3)and(xbal<jou2x-3)and(xbal>jou2x-8)and(vecxbal>0) then begin vecybal:=-2; vecxbal:=-1; end;
if (ybal>jou2y+37)and(ybal<=jou2y+43)and(xbal<jou2x-3)and(xbal>jou2x-8)and(vecxbal>0) then begin vecybal:=2; vecxbal:=-1; end;
if (ybal>jou2y-8)and(ybal<=jou2y+48)and(xbal<jou2x-3)and(xbal>jou2x-8) then
begin
turbo:=0;
if ttturbo=2 then begin turbo:=1; efturbo:=1; vecxbal:=-2; end;
end;

if xbal>=625 then begin vecxbal:=-1; turbo:=0; scorj1:=scorj1+1;
     if vecybal>0 then
     begin nbre:=vecybal; vecybal:=trunc(nbre/nbre); end
     else
     begin nbre:=vecybal; vecybal:=-trunc(nbre/nbre); end;
     end;
if xbal<=15 then begin vecxbal:=1; turbo:=0; scorj2:=scorj2+1;
     if vecybal>0 then
     begin nbre:=vecybal; vecybal:=trunc(nbre/nbre); end
     else
     begin nbre:=vecybal; vecybal:=-trunc(nbre/nbre); end;
     end;
if ybal>=465 then begin nbre:=vecybal*-1; vecybal:=trunc(nbre); end;
if ybal<=15 then begin nbre:=vecybal*-1; vecybal:=trunc(nbre); end;

if (jou1y=15)and(vecyjou1=-1) then begin vecyjou1:=0; end;
if (jou1y=425)and(vecyjou1=1) then begin vecyjou1:=0; end;
if (jou1x=15)and(vecxjou1=-1) then begin vecxjou1:=0; end;
if (jou1x=315)and(vecxjou1=1) then begin vecxjou1:=0; end;

if (jou2y=15)and(vecyjou2=-1) then begin vecyjou2:=0; end;
if (jou2y=425)and(vecyjou2=1) then begin vecyjou2:=0; end;
if (jou2x=325)and(vecxjou2=-1) then begin vecxjou2:=0; end;
if (jou2x=625)and(vecxjou2=1) then begin vecxjou2:=0; end;

END;

PROCEDURE Effacer;
BEGIN
setcolor(0);
testjou1x:=testjou1x+vecxjou1;
testjou1y:=testjou1y+vecyjou1;
testjou2x:=testjou2x+vecxjou2;
testjou2y:=testjou2y+vecyjou2;
if (testjou1x<>jou1x)or(testjou1y<>jou1y) then
     begin
     line(jou1x,jou1y,jou1x,jou1y+40);
     end;
if (testjou2x<>jou2x)or(testjou2y<>jou2y) then
     begin
     line(jou2x,jou2y,jou2x,jou2y+40);
     end;
if turbo=0 then
begin
circle(xbal,ybal,5);
end;
END;

PROCEDURE titre;
BEGIN
setcolor(10);
settextstyle(10,0,3);
outtextxy(260,100,'PONG');
readkey;
setcolor(0);
outtextxy(260,100,'PONG');
settextstyle(0,0,0);
END;

PROCEDURE Changement;
BEGIN
xbal:=xbal+vecxbal;
ybal:=ybal+vecybal;
jou1x:=jou1x+vecxjou1;
jou1y:=jou1y+vecyjou1;
jou2x:=jou2x+vecxjou2;
jou2y:=jou2y+vecyjou2;
END;

BEGIN
opengraph;
logo;
titre;
parametre;
1:terrain;
delay(1);
if keypressed then begin touche; end;
if auto=1 then
begin
ordi;
end;
bord;
effacer;
changement;
goto 1;
END.
