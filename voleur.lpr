{$mode fpc}
PROGRAM voleur;
uses graph,crt,dos,eric;
label 1,2,3;
var mot,obj:string;
    condition,action,piece,passe1,passe2,passe3,passe4,ent:integer;
FUNCTION chaine(p:INTEGER):STRING;
VAR c:STRING;
BEGIN
     STR(p,c);
     chaine:=c;
END;



PROCEDURE parametre;
          BEGIN
          condition:=0; action:=0; piece:=0; passe1:=0; passe2:=0; passe3:=0; passe4:=0; ent:=0;
          END;

PROCEDURE boite;
          BEGIN
          setcolor(15);
          rectangle(0,0,639,479);
          SetFillStyle(1,15);
          bar(0,0,450,150);
          bar(450,0,640,480);
          SetFillStyle(1,7);
          bar(2,2,447,147);
          setfillstyle(1,9);
          bar(452,2,637,477);
          SetFillStyle(1,15);
          bar(5,5,444,144);
          SetFillStyle(1,0);
          bar(8,8,441,141);
          SetFillStyle(1,15);
          bar(8,97,441,103);
          outtextxy(485,50,('LES COMMANDES'));
          outtextxy(495,65,('PRINCIPALES'));
          outtextxy(480,85,('_ EXAMINER'));
          outtextxy(480,95,('_ PRENDRE'));
          outtextxy(480,105,('_ UTILISER'));
          outtextxy(480,115,('_ ALLUMER'));
          outtextxy(480,125,('_ ETEINDRE'));
          outtextxy(480,135,('_ OUVRIR'));
          outtextxy(480,145,('_ FERMER'));
          outtextxy(480,155,('_ MONTER'));
          outtextxy(480,165,('_ DESCENDRE'));
          outtextxy(525,330,('INFOS'));
          outtextxy(458,350,('TAPEZ EXIT POUR SORTIR'));
          outtextxy(458,360,('N''UTILISEZ PAS :'));
          outtextxy(458,370,('  _ LES MAJUSCULES'));
          outtextxy(458,380,('  _ LES PRONOMS'));
          outtextxy(458,390,('  _ LA PONCTUATION'));
          outtextxy(458,400,('  _ LES ACCENTS'));
          outtextxy(458,410,('VALIDEZ CHAQUE ACTION'));
          outtextxy(458,440,('P.S: SI VOUS ARRIVEZ'));
          outtextxy(458,450,(' A JOUER, VOUS ETES'));
          outtextxy(458,460,('     UN BON !!!'));
          line(485,60,585,60);
          line(485,75,585,75);
          line(525,340,562,340);
          line(545,200,545,300);
          line(495,250,595,250);
          moveto(545,200);
          lineto(535,240);
          lineto(495,250);
          lineto(535,260);
          lineto(545,300);
          lineto(555,260);
          lineto(595,250);
          lineto(555,240);
          lineto(545,200);
          setfillstyle(1,2);
          floodfill(540,248,15);
          setfillstyle(1,12);
          floodfill(540,252,15);
          setfillstyle(1,14);
          floodfill(550,252,15);
          SetFillStyle(1,7);
          floodfill(550,242,15);
          outtextxy(530,190,('NORD'));
          outtextxy(535,305,('SUD'));
          outtextxy(455,248,('OUEST'));
          outtextxy(600,248,('EST'));
          setcolor(5);
          outtextxy(460,20,('Si t''es nul tape aide'));
          bar(5,99,444,101);
          window(3,2,54,6);
          END;

PROCEDURE texte;
          BEGIN
          textcolor(0);
          clreol;
          textcolor(15);
          readln(mot);
          END;

PROCEDURE objet;
          BEGIN
          SetFillStyle(1,0);
          bar(8,104,441,141);
          if piece=0 then
          begin
               if action=1 then
               begin
                  if obj=('porte') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Elle est ferme  clef.');
                  end;
                  if obj=('fenetre') then
                  begin
                       if passe1=0 then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Elle est ferme de l''intrieur.');
                       end;
                       if passe1=1 then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Elle est ouverte');
                       end;
                  end;
                  if obj=('caniveau') then
                  begin
                       if passe2=0 then
                       begin
                       passe3:=1;
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Il y a une clef, un tournevis et un battonet de glace.');
                       end;
                       if passe2=1 then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Il y a une clef et un tournevis.');
                       end;
                  end;
                  if passe3=1 then
                  begin
                       if obj=('clef') then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Elle est dans une merde de chien !!!');
                       end;
                       if obj=('tournevis') then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'C''est un tournevis en plastique pour bb !!!');
                       end;
                       if obj=('battonet') then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Tu vas pas me croire, c''est un battonet de glace !!!');
                       end;
                  end;
               end;


               if action=2 then
               begin
               if obj=('porte') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Au voleur !!! Au voleur !!!');
                  end;

                  if obj=('fenetre') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Tu veux te la prendre,espce de "menuisierrophile" !!!');
                  end;

                  if passe3=1 then
                  begin
                       if (obj=('clef')) then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Tu peux pas la prendre !!! En la regardant mieux');
                       outtextxy(10,120,'tu comprendras pourquoi !!!');
                       end;
                       if obj=('tournevis') then
                       begin
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Il ne va pas te servir !!! Regardes le mieux !!!');
                       end;
                       if ((obj=('battonet'))and(passe2=0)) then
                       begin
                       passe2:=1;
                       SetFillStyle(1,0);
                       bar(8,104,441,141);
                       outtextxy(10,110,'Vous avez pris le battonet !!!');
                       end;
                  end;
               end;
               if action=3 then
               begin
                if obj=('porte') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'MAC GYVER je te donne quatres planches');
                  outtextxy(10,120,'et tu me fais une table ???');
                  end;
                if obj=('fenetre') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Et la porte t''as essay ???');
                  end;
                if ((obj=('battonet'))and(passe4=1)) then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Tu as ouvert la fentre !!!!!!!!!');
                  passe1:=1;
                  end;
               end;
               if action=4 then
               begin
               if (obj=('porte'))or(obj=('fenetre')) then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Espce de Piroman !!!');
                  end;

               end;
               if action=5 then
               begin
               if obj=('porte') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Ho le pompier! Avant d''teindre il faut l''allumer!');
                  end;
               if obj=('fenetre') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'T''es bien ou c''est de naissance !!!');
                  end;
               end;
               if action=6 then
               begin
               if obj=('porte') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Tu peux pas c''est ferm  clef.');
                  end;
               if obj=('fenetre') then
                  begin
                  if passe1=0 then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Bien pens mais comment ???');
                  if passe2=1 then
                     begin
                     textcolor(0);
                     clreol;
                     textcolor(15);
                     readln(mot);
                     if mot=('utiliser') then
                     begin
                     action:=3;
                     SetFillStyle(1,0);
                     bar(8,104,441,141);
                     outtextxy(10,110,'Utiliser quel objet ?');
                     textcolor(0);
                     clreol;
                     textcolor(15);
                     readln(obj);
                     passe4:=1;
                     objet;
                     end;
                     end;
                  end;
                  if passe1=1 then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'La fentre est ouverte !!!');
                  end;
                  end;
               end;
               if action=7 then
               begin
               if obj=('porte') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Et ta bouche , tu la ferme !!!');
                  end;
               if obj=('fenetre') then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Le but c''est de rentrer !!!');
                  end;
               end;
               if action=8 then
               begin
               end;
               if action=9 then
               begin
               if (obj=('porte'))or(obj=('fenetre')) then
                  begin
                  SetFillStyle(1,0);
                  bar(8,104,441,141);
                  outtextxy(10,110,'Du calme Cow-boy !!!');
                  end;
               end;
          end;
          action:=0;
          passe4:=0;
          END;

PROCEDURE reponse;
          BEGIN
          if mot=('exit') then
             Begin
             condition:=1;
             end;
          if mot=('examiner') then
             begin
             action:=1;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Examiner quel objet ?');
             textcolor(0);
             clreol;
             textcolor(15);
             readln(obj);
             objet;
             end;
          if mot=('prendre') then
             begin
             action:=2;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Prendre quel objet ?');
             textcolor(0);
             clreol;
             textcolor(15);
             readln(obj);
             objet;
             end;

           if mot=('utiliser') then
             begin
             action:=3;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Utiliser quel objet ?');
             textcolor(0);
             clreol;
             textcolor(15);
             readln(obj);
             objet;
             end;

            if mot=('allumer') then
             begin
             action:=4;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Allumer quoi ?');
             textcolor(0);
             clreol;
             textcolor(15);
             readln(obj);
             objet;
             end;

             if mot=('eteindre') then
             begin
             action:=5;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Eteindre quoi ?');
             textcolor(0);
             clreol;
             textcolor(15);
             readln(obj);
             objet;
             end;

             if mot=('ouvrir') then
             begin
             action:=6;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Ouvrir quoi ?');
             textcolor(0);
             clreol;
             textcolor(15);
             readln(obj);
             objet;
             end;

             if mot=('fermer') then
             begin
             action:=7;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Fermer quoi ?');
             textcolor(0);
             clreol;
             textcolor(15);
             readln(obj);
             objet;
             end;

             if mot=('monter') then
             begin
             action:=8;
             SetFillStyle(1,0);
             bar(8,104,441,141);
             end;

             if mot=('descendre') then
             begin
             action:=9;
             SetFillStyle(1,0);
             bar(8,104,441,141);

             end;

             if mot=('aide') then
             begin
             if piece=0 then
             begin
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'  veaunica allant vers ... ');
             end;
             end;

             if mot=('nord') then
             begin
             if (piece=0)and(passe1=1) then
             begin
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Vous tes dans la maison ...');
             piece:=1;
             ent:=1;
             end;
             end;

             if mot=('sud') then
             begin
             if piece=1 then
             begin
             SetFillStyle(1,0);
             bar(8,104,441,141);
             outtextxy(10,110,'Vous tes dehors ...');
             piece:=0;
             ent:=1;
             end;
             end;

          END;

PROCEDURE dehors;
          BEGIN
          SetFillStyle(1,0);
          bar(1,150,449,478);
          SetFillStyle(1,15);
          bar(383,448,386,455);
          bar(389,448,392,455);
          bar(395,448,398,455);
          bar(401,448,404,455);
          bar(407,448,410,455);
          bar(413,448,416,455);
          setcolor(15);
          line(1,380,449,380);
          line(1,430,449,430);
          line(1,455,380,455);
          line(420,455,449,455);
          line(380,455,380,445);
          line(420,455,420,445);
          line(380,445,420,445);
          line(1,460,449,460);
          line(80,380,80,360);
          line(185,380,185,360);
          line(80,360,185,360);
          line(90,200,90,360);
          line(90,200,175,200);
          line(175,200,175,360);
          rectangle(280,210,360,290);
          rectangle(240,210,400,290);
          line(320,210,320,290);
          rectangle(285,215,315,285);
          rectangle(325,215,355,285);
          circle(165,280,4);
          SetFillStyle(1,8);
          floodfill(10,450,15);
          SetFillStyle(1,7);
          floodfill(10,461,15);
          SetFillStyle(1,6);
          floodfill(10,400,15);
          SetFillStyle(1,1);
          floodfill(150,280,15);
          SetFillStyle(1,4);
          floodfill(10,350,15);
          SetFillStyle(1,11);
          floodfill(290,220,15);
          floodfill(330,220,15);
          SetFillStyle(1,12);
          floodfill(250,220,15);
          floodfill(390,220,15);
          line(1,440,449,440);
          rectangle(100,210,165,270);
          rectangle(100,290,165,350);
          END;


PROCEDURE piece1;
          BEGIN
          SetFillStyle(1,0);
          bar(1,150,449,478);
          end;

BEGIN
     opengraph;
     parametre;
     boite;
   3:ent:=0;
     if piece=0 then begin dehors; end;
     if piece=1 then begin piece1; end;
   1:if condition=1 then begin goto 2 end;
     texte;
     reponse;
     if ent=1 then begin goto 3; end;
     goto 1;
2:END.
